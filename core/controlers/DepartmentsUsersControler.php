<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DepartmentsUsers
 *
 * @author jakub
 */
class DepartmentsUsersControler extends Controler {

//put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if (!DepartmentsUtils::gI()->isUserAdminAnywhere()) {
            $this->redirectToError("You are not admin in any department", 401);
        }
        if ($URL_params[0] == "invite-user") {
            $this->inviteUser($URL_params);
        } else if ($URL_params[0] == "create-user") {
            $this->create_user($URL_params);
        } elseif (is_numeric($URL_params[0])) {
            $this->user($URL_params);
        } else {
            $this->users();
        }
    }

    public function users() {
        $departments_admin = DepartmentsUtils::gI()->getUserDepartments(User::getUserId(), true);
        $users = User::getInstance()->getUsersInDepartments($departments_admin);
        foreach ($users as $usr) {
            $usr['departments'] = DepartmentsUtils::gI()->getUserDepartments($usr['user_internal_id']);
            $this->data['users'][] = $usr;
        }
        foreach ($departments_admin as $dep) {
            if ($dep['user_role'] == 3 || $dep['user_role'] > 4) {
                $this->data['departments_head'][$dep['department_id']] = $dep;
            }
            if ($dep['user_role'] > 1) {
                $this->data['departments_admin'][$dep['department_id']] = $dep;
            }
        }
        bdump($departments_admin);
        $this->view = "department_users";
    }

    public function inviteUser($URL_params) {
        $department_admin = DepartmentsUtils::gI()->getOneByUser($_POST['department'], User::getUserId());
        if ($department_admin['user_role'] < 2) {
            $this->redirectToError("You can not invite other users to this department", 401);
        }
        $user = User::getInstance()->getOneUsersByLoginName($_POST['username']);
        if (!isset($user['login_name'])) {
            $this->addMessage("User not exist!", "danger");
            $this->redirect("departments-users");
        }
        $department_user = DepartmentsUtils::gI()->getOneByUser($_POST['department'], $user['user_internal_id']);
        if (isset($department_user['user_role'])) {
            $this->addMessage("User is already in selected department!", "primary");
            $this->redirect("departments-users");
        }
        $department_invitation = DepartmentsUtils::gI()->getInvitationsByUser($user['user_internal_id'], $department_admin['department_id'], "pending");
        if (isset($department_invitation[0]['user_role'])) {
            $this->addMessage("This user is already invited to join this department", "danger");
            $this->redirect("departments-users");
        }
        $selected_role = $_POST['role'];
        if (($department_admin['user_role'] == 2 && $selected_role > 1) ||
                $department_admin['user_role'] < $selected_role) {
            $this->addMessage("You cann´t choose this role.", "danger");
            $this->addMessage("Role has been adjusted", "primary");
            $selected_role = $department_admin['user_role']--;
        }
        DepartmentsUtils::gI()->createInvitation($user['user_internal_id'],
                $department_admin['department_id'], $selected_role, User::getUserId());
        $this->addMessage("User has been invited", "success");
        $this->redirect("departments-users");
    }

    public function create_user($URL_params) {
        $department_admin = DepartmentsUtils::gI()->getOneByUser($_POST['department'], User::getUserId());
        if ($department_admin['user_role'] < 3) {
            $this->redirectToError("You can not create users in selected department", 401);
        }
        if (User::getInstance()->isUsernameOrEmailExist($_POST['login_name'], $_POST['email'])) {
            $this->redirectToError("User with this name or email already exist!", 409);
        }
        $selected_role = $_POST['role'];
        if (($department_admin['user_role'] == 2 && $selected_role > 1) ||
                $department_admin['user_role'] < $selected_role) {
            $this->addMessage("You cann´t choose this role.", "danger");
            $this->addMessage("Role has been adjusted", "primary");
            $selected_role = $department_admin['user_role']--;
        }
        $id = User::getInstance()->createUser($_POST['login_name'], $_POST['email'], $_POST['locale']);
        User::getInstance()->addPortalId($id);
        DepartmentsUtils::gI()->addToUser($id, $_POST['department'], $selected_role);
        User::getInstance()->rebaseRules();
        $this->addMessage("User has been created", "success");
        $this->addMessage("Email with set password token has been queued");
        $this->redirect("departments-users/" . $id . "/profile");
    }

    public function user($URL_params) {
        if (isset($URL_params[1])) {
            if ($URL_params[1] == "rights") {
                $this->rights($URL_params);
            } else if ($URL_params[1] == "profile") {
                $this->profile($URL_params);
            } else if ($URL_params[1] == "log") {
                $this->log($URL_params);
            }
        }
    }

    public function rights($URL_params) {
        if (!DepartmentsUtils::gI()->canUserEditUser(User::getUserId(), $URL_params[0], "rights")) {
            $this->redirectToError("You can not edit rights for this user", 401);
        }
        $this->data['rights'] = DepartmentsUtils::gI()->getRightsForUser(User::getUserId(), $URL_params[0]);
        $this->data['user_edit'] = User::getInstance()->getOneUser($URL_params[0]);
        if ($URL_params[2] == "manage_rights") {
            $this->manage_rights($URL_params);
        } elseif ($URL_params[2] == "in_app_rights") {
            $this->remoteRights($URL_params);
        }
        $this->view = "department_user_rights";
    }

    public function manage_rights($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        if (array_key_exists($_POST['data'], $this->data['rights'])) {
            RightsUtils::gI()->updateUserRight($URL_params[0], $_POST['data'],
                    "access", $_POST['action'] == "add_right" ? 1 : 0);
            if ($_POST['action'] == "add_right") {
                try {
                    AppsUtils::gI()->registerUserInApp($URL_params[0], str_replace("app_", "", $_POST['data']));
                    $this->addMessage("User has been registered in app!", "success");
                } catch (RemoteRigistrationException $exc) {
                    $this->addMessage($exc->getMessage(), "danger");
                    $this->addMessage("Registration may not be completed! Try it again later or contact app´s responsible admin.", "secondary");
                }
            } else if ($_POST['action'] == "remove_right") {
                try {
                    AppsUtils::gI()->disableUserInApp($URL_params[0], str_replace("app_", "", $_POST['data']));
                    $this->addMessage("User has been disabled in app!", "success");
                } catch (RemoteRigistrationException $exc) {
                    $this->addMessage($exc->getMessage(), "danger");
                    $this->addMessage("Action may not be completed! Action will be handled with maintenance cron", "secondary");
                }
            } else {
                $this->redirectToError("Wrong action", 400);
            }
            $this->addMessage("Action saved!", "success");
            $this->redirect("departments-users/" . $URL_params[0] . "/rights");
        } else {
            $this->redirectToError("You can not change this right!", 401);
        }
    }

    public function remoteRights($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        if (!array_key_exists($_POST['data'], $this->data['rights'])) {
            $this->redirectToError("You can not manage right in this app", 401);
        }
        $app = AppsUtils::gI()->getAppByCode(str_replace("app_", "", $_POST['data']));
        $rights = RightsUtils::gI()->getRemoteRights($app, $URL_params[0]);
        $parsed_rights = RightsUtils::gI()->parseFromPostByOriginal($rights, User::getInstance()->getAdminLevel() > 3 ? true : false);
        bdump($parsed_rights, "parsed_rights");
        RightsUtils::gI()->setRemoteRights($_POST['data'], $URL_params[0], $parsed_rights);
        $this->addMessage("User rights in app has been updated!", "success");
        $this->redirect("departments-users/" . $URL_params[0] . "/rights");
    }

    public function profile($URL_params) {
        if (!DepartmentsUtils::gI()->canUserEditUser(User::getUserId(), $URL_params[0], "profile")) {
            $this->redirectToError("You can not edit rights for this user", 401);
        }
        if ($URL_params[2] == "update_profile") {
            $this->updateProfile($URL_params);
        }
        if ($URL_params[2] == "reset-password") {
            $this->resetPassword($URL_params);
        }

        $this->data['user_edit'] = User::getOneUser($URL_params[0]);
        $this->data['user_data'] = User::getInstance()->getUserData($URL_params[0]);
        $this->data['tokens'] = TokenUtils::getTokensByUserId($URL_params[0], time() - 100000);
        $this->data['sessions'] = SessionsUtils::get_instance()->
                getSessionsByUserId($URL_params[0], date("Y-m-d H:i:s", time() - 100000));
        $this->data['enable_login_as'] = User::getInstance()->getRuleValue("admin_login", "admins");
        if (USINGDEPARTMENTS) {
            $this->data['departemnts'] = DepartmentsUtils::gI()->getUserDepartments($URL_params[0]);
        }
        $this->view = "department_user_profile";
    }

    public function updateProfile($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        $editable = array("email", "nickname", "locale", "real_name", "phone", "address", "country", "city", "company", "gender");
        User::getInstance()->updateUserData(array_intersect_key($_POST, array_flip($editable)), $URL_params[0]);
        $this->addMessage("Profile saved!");
        $this->redirect("departments-users/" . $URL_params[0] . "/profile");
    }

    public function resetPassword($URL_params) {
        $user = User::getOneUser($URL_params[0]);
        if (!isset($user['mail']) && !$user['mail'] == "") {
            $this->addMessage("User has not mail. You have to contact developer to reset user password");
            $this->redirect("departments-users/" . $URL_params[0] . "/profile");
        }
        $token = TokenUtils::get_instance()->createPasswordResetToken($URL_params[0], User::getUserId(), "department_head");
        $to = $user['mail'];
        $message = '
<html>
<head>
  <title>Password reset token - %app_name%</title>
</head>
<body>
  <p>Hello,</p>
  <p>your head requested password reset token for you!</p>
  <p><a href="%url%">
  %url%
  </a>
  </p><br />
  Sincerelly,
  Your LDAP bot.
</body>
</html>
';
        $url = "https://" . SESSIONDOMAIN . "/password-restore/token/" . $token . "/" . $URL_params[0];
        $subject = Lang::str("Password reset token - ") . SettingsUtils::gI()->getSett("SYSTEM", "app_visible_name");
        $message = Lang::strVar($message, array("app_name" => SettingsUtils::gI()->getSett("SYSTEM", "app_visible_name"), "url" => $url));

// To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';
// Additional headers
        $headers[] = 'To: <' . $user['mail'] . '>';
        $headers[] = 'From: ' . SettingsUtils::gI()->getSett("SYSTEM", "app_visible_name") . ' <system@topescape.cz>';

        $status = mail($to, $subject, $message, implode("\r\n", $headers));
        if ($status) {
            $this->addMessage("Mail with password reset instruction has been sent to user!", "success");
        }
        $this->addMessage("There is some problem, please report this to developer!", "danger");
        $this->addMessage(error_get_last()['message']);
        $this->redirect("departments-users/" . $URL_params[0] . "/profile");
    }

}
