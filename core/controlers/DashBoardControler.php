<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class DashBoardControler extends Controler {

    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, true, 0);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if (isset($URL_params[0])) {
            $this->actions($URL_params);
        }
        $this->view = "dashBoard_new";
        $apps = AppsUtils::getAllApps($this->userControler->getloggedUserId());
        $this->data['invitations'] = DepartmentsUtils::getInstance()->getInvitationsByUser(User::getUserId(), null, "pending");
        foreach ($apps as $key => $app) {
            if ($this->userControler->getAdminLevel() < 4 &&
                    ($this->userControler->getRuleValue("app_" . $app['app_code'], "access") == 0 || $app['disabled'] == 1)) {
                unset($apps[$key]);
            }
            if ($this->userControler->getAdminLevel() > 3 && !$app['external'] && $app['has_translations']) {
                $apps[$key]['can_user_edit_translations'] = 1;
            }
        }
        $this->data['apps'] = ArrayUtils::makeKeyCategoryArray($apps, "group_code");
        $this->data['apps_right'] = User::getInstance()->getRuleValue("apps", "view");
    }

    public function actions($URL_params) {
        if ($URL_params[0] == "invitations") {
            $this->invitations($URL_params);
        }
    }

    public function invitations($URL_params) {
        $inv = DepartmentsUtils::gI()->getOneInvitationById($URL_params[1], User::getUserId());
        if ($inv['invitation_status'] == "pending") {
            if ($URL_params[2] == "accept") {
                DepartmentsUtils::gI()->addToUser($inv['user_internal_id'],
                        $inv['department_id'], $inv['user_role']);
                DepartmentsUtils::gI()->updateInvitaion($inv['invitation_id'], "accepted");
                $this->addMessage("Invitations has been accepted.", "success");
                $this->addMessage("Welcome to additional department", "priamry");
                $this->redirect("dashBoard");
            } else if ($URL_params[2] == "reject") {
                DepartmentsUtils::gI()->updateInvitaion($inv['invitation_id'], "rejected");
                $this->addMessage("Invitations has been rejected.", "success");
                $this->redirect("dashBoard");
            }
        }
    }

}
