<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuthControler
 *
 * @author jakub
 */
class AuthControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->view = "app_login";
        $this->data['can_login'] = false;
        if (isset($_GET['redirect'])) {
            $_SESSION['temp']['redirect'] = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], "redirect=") + 9);
        } else {
            $_SESSION['temp']['redirect'] = null;
        }
        $app = AppsUtils::gI()->getAppByCode($URL_params[0]);
        if (!isset($app['id'])) {
            $this->addMessage("App you requested doesn´t exists or is disabled", "danger");
            //$this->redirect(SettingsUtils::gI()->getSett("SYSTEM", "landing_page"));
        } else {
            if (User::getInstance()->getRuleValue("app_" . $app['app_code'], "access")) {
                $this->data['can_login'] = true;
            } else {
                $this->addMessage("You do not have rights to login into this app!", "danger");
            }
        }
        $this->data['remote_app_name'] = $app['name'];
        if (isset($_POST['action'])) {
            $this->do_login($app);
        }
    }

    public function do_login($app) {
        if ($this->data['can_login']) {
            CSRFUtils::gI()->checkCSRF($_POST['csrf']);
            if ($app['external']) {
                header("Location: " . $app['url']);
            } else {
                $token = TokenUtils::get_instance()->createTokenForRemoteLdapLogin(
                        User::getUserId(), $app['app_code']);
                if ($_SESSION['temp']['redirect'] != null) {
                    header("Location: " . $app['url'] . "/login/ldap/" . $token . "/?redirect=" . $_SESSION['temp']['redirect']);
                } else {
                    header("Location: " . $app['url'] . "/login/ldap/" . $token);
                }
            }
            header("Connection: close");
            $_SESSION['temp']['redirect'] = null;
            exit;
        }
    }

}
