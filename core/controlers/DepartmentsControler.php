<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DepartmentsControler
 *
 * @author jakub
 */
class DepartmentsControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if (!DepartmentsUtils::gI()->isUserAdminAnywhere()) {
            $this->redirectToError("You are not admin in any department", 401);
        }
        if ($URL_params[0] == "all") {
            $this->all($URL_params);
        } else if (is_numeric($URL_params[0])) {
            $this->one($URL_params);
        } else {
            $this->my($URL_params);
        }
        $this->data['can_edit'] = User::get_instance()->getRuleValue("departments", "edit");
        $this->data['can_create'] = User::get_instance()->getRuleValue("departments", "create");
    }

    public function my($URL_params) {
        $this->data['departments'] = DepartmentsUtils::gI()->getUserDepartments(User::getUserId());
        $this->view = "departments";
    }

    public function all($URL_params) {
        if (isset($_POST['csrf']) && $URL_params[1] == "create_new") {
            $this->createNew($URL_params);
        }
        if (User::get_instance()->getRuleValue("departments", "view")) {
            $this->data['departments'] = DepartmentsUtils::gI()->getAll();
        } else {
            $this->data['departments'] = DepartmentsUtils::gI()->getUserDepartments(User::getUserId());
        }
        bdump($this->data['departments']);
        $this->view = "departments";
    }

    public function one($URL_params) {
        if ($URL_params[1] == "invitaion_withdraw") {
            $this->withdrawInvitaion($URL_params);
        } else if ($URL_params[1] == "remove_user") {
            $this->removeUser($URL_params);
        } else if ($URL_params[1] == "change_role") {
            $this->changeUserRole($URL_params);
        }
        $this->data['department'] = DepartmentsUtils::gI()->getOne($URL_params[0]);
        $this->data['users_in_department'] = DepartmentsUtils::gI()->getUsersInDepartment($URL_params[0]);
        $this->data['apps_in_department'] = DepartmentsUtils::gI()->getAppsInDepartment($URL_params[0]);
        $this->data['invitations'] = DepartmentsUtils::gI()->getInvitationsByDepartment($URL_params[0]);
        bdump(MysqliDb::getInstance()->getLastQuery());
        bdump($this->data);
        $this->view = "department";
    }

    public function withdrawInvitaion($URL_params) {
        $admin_dep = DepartmentsUtils::gI()->getOneByUser($URL_params[0], User::getUserId());
        if ($admin_dep['user_role'] < 2) {
            $this->redirectToError("You can not withdraw this invitaion!", 401);
        }
        DepartmentsUtils::gI()->updateInvitaion($URL_params[2], "withdraw");
        $this->addMessage("Invitation has been withdrawed!", "success");
        $this->redirect("departments/" . $URL_params[0]);
    }

    public function removeUser($URL_params) {
        $admin_dep = DepartmentsUtils::gI()->getOneByUser($URL_params[0], User::getUserId());
        $user_dep = DepartmentsUtils::gI()->getOneByUser($URL_params[0], $URL_params[2]);
        if ($admin_dep['user_role'] < 2 || $user_dep['user_role'] > $admin_dep['user_role']) {
            $this->redirectToError("You can not remove this user from department", 401);
        }
        DepartmentsUtils::gI()->removeUser($URL_params[2], $URL_params[0]);
        $this->checkRemovedUser($URL_params[2], $URL_params[0]);
        $this->addMessage("User has been removed from department", "success");
        $this->redirect("departments/" . $URL_params[0]);
    }

    public function checkRemovedUser($user_id, $dep_id) {
        $dep = DepartmentsUtils::gI()->getUserDepartments($user_id);
        if (count($dep) == 0) {
            User::getInstance()->disableUser($user_id, true);
            TokenUtils::get_instance()->invalidAllTokensByUserId($URL_params[0]);
            SessionsUtils::get_instance()->invalidAllSessionsByUserId($URL_params[0]);
        } else {
            $apps = DepartmentsUtils::gI()->getAppsInDepartment($dep_id);
            foreach ($apps as $app) {
                if ($app['using_departments']) {
                    throw new Exception("missing implementation");
                } else {
                    RightsUtils::gI()->updateUserRight($user_id, "app_" . $app['app_code'], "access", 0);
                }
            }
        }
    }

    public function changeUserRole($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        $admin_dep = DepartmentsUtils::gI()->getOneByUser($URL_params[0], User::getUserId());
        if ($admin_dep['user_role'] < 3) {
            $this->redirectToError("You cannot change role of this user", 401);
        }
        $user_role = $_POST['admin_level'] > $admin_dep['user_role'] ? $admin_dep['user_role'] - 1 : $_POST['admin_level'];
        DepartmentsUtils::gI()->updateUserLevel($URL_params[0], $URL_params[2], $user_role);
        $this->addMessage("Role has been changed!", "success");
        $this->redirect("departments/" . $URL_params[0]);
    }

    public function createNew($URL_params) {
        if (User::get_instance()->getRuleValue("departments", "create") == 0) {
            $this->redirectToError("You cannot create departments", 401);
        }
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        try {
            $id = DepartmentsUtils::gI()->createNew($_POST['name'], $_POST['code']);
            $this->addMessage("Department has been created", "success");
            DepartmentsUtils::gI()->addToUser(User::getUserId(), $id, 3);
            $this->addMessage("You have been added to this department as head.", "primary");
            $this->redirect("departments/" . $id);
        } catch (DepartmentException $exc) {
            $this->addMessage("Department has not been created!", "danger");
            $this->addMessage($exc->getMessage(), "warning");
            $this->redirect("departments/all");
        }
    }

}
