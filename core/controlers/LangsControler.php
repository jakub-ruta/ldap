<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LangsControler
 *
 * @author jakub
 */
class LangsControler extends Controler {

    public function execute($URL_params) {
        $this->initWithRule("apps", "edit", $URL_params);
        $this->defaultTemplate = false;
        $this->Template = "template_new";

        if ($URL_params[0] == "new") {
            TranslationsUtils::addNewLang(array("app_id" => $_GET['app_id'], "lang_name" => $_POST['lang_name'], "lang_code" => $_POST['lang_code']));
            $this->addMessage("Lang has been added", "success");
            $this->redirect("langs");
        } elseif (is_numeric($URL_params[0])) {
            $this->lang($URL_params);
        } else {
            $this->view = "langs";
            $this->data['langs'] = TranslationsUtils::getAllLangs();
        }
    }

    public function lang($URL_params) {
        if (isset($_POST['lang_name'])) {
            TranslationsUtils::updateLang($URL_params[0], array("lang_name" => $_POST['lang_name'], "lang_code" => $_POST['lang_code'],
                "lang_enabled" => $_POST['lang_enabled'], "lang_can_be_uploaded" => $_POST['lang_can_be_uploaded']));
            $this->addMessage("Lang has been updated", "success");
            $this->redirect("langs/" . $URL_params[0]);
        }
        $this->view = "lang";
        $this->data['lang'] = TranslationsUtils::getLang($URL_params[0]);
    }

}
