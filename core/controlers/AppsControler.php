<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class AppsControler extends Controler {

    public function execute($URL_params) {
        $this->initWithRule("apps", "view", $URL_params);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if ($URL_params[0] == "generate_new_token") {
            $this->generateToken();
        } elseif (isset($URL_params[0])) {
            $this->app($URL_params);
        } else {
            $this->view = "apps";
            $this->data['apps'] = AppsUtils::getAllApps($this->userControler->getloggedUserId());
            bdump($this->data['apps']);
        }
    }

    public function app($URL_params) {
        if ($URL_params[1] == "addStaff" && $_POST['staff_id']) {
            AppsUtils::add_staff($URL_params[0], $_POST['staff_id']);
            $this->addMessage("User has been added as responsible staff.", "success");
            $this->addMessage("Do not forgot to fill position!", "secondary");
            $this->redirect("apps/" . $URL_params[0]);
        } elseif ($URL_params[1] == "add-department") {
            $this->addDepartment($URL_params);
        } elseif ($URL_params[1] == "change-group") {
            $this->changeGroup($URL_params);
        } elseif ($URL_params[1] == "remove_department") {
            $this->removeDepartment($URL_params);
        } else if ($URL_params[1] == "deleteStaff" && is_numeric($URL_params[2])) {
            AppsUtils::gI()->delete_staff($URL_params[0], $URL_params[2]);
            $this->addMessage("User has been removed as responsible staff.", "success");
            $this->redirect("apps/" . $URL_params[0]);
        } else if (isset($_POST['csrf'])) {
            $this->editApp($URL_params);
        }
        $this->data['app_editable_fields'] = AppsUtils::gI()->editable;
        $this->data['app'] = AppsUtils::getAppById($URL_params[0]);
        $this->data['app_in_departments'] = DepartmentsUtils::gI()->getDepartmentsForApp($URL_params[0]);
        $this->data['app_in_group'] = AppsUtils::gI()->getAppGroup($URL_params[0]);
        $this->data['responsible_staff'] = AppsUtils::getResponsibleStaffByAppIdAll($URL_params[0]);
        $this->data['langs'] = TranslationsUtils::getAppLangsByAppId($URL_params[0]);
        $this->data['users'] = User::getInstance()->getAllUsers();
        $this->data['all_groups'] = AppsUtils::gI()->getAllGroups();
        $this->data['all_departments'] = DepartmentsUtils::gI()->getAll();
        bdump($this->data);
        $this->view = "app";
    }

    public function addDepartment($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        DepartmentsUtils::gI()->addToApp($URL_params[0], $_POST['department_id']);
        $this->addMessage("App has been added to department.", "success");
        $this->redirect("apps/" . $URL_params[0]);
    }

    public function changeGroup($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        AppsUtils::gI()->changeAppGroup($URL_params[0], $_POST['group_id']);
        $this->addMessage("App group has been changed.", "success");
        $this->redirect("apps/" . $URL_params[0]);
    }

    public function removeDepartment($URL_params) {
        DepartmentsUtils::gI()->RemoveFromApp($URL_params[0], $URL_params[2]);
        $this->addMessage("App has been removed from department.", "success");
        $this->redirect("apps/" . $URL_params[0]);
    }

    public function editApp($URL_params) {
        AppsUtils::gI()->updateApp($URL_params[0], $_POST);
        $this->addMessage("App info has been updated.", "success");
        $this->redirect("apps/" . $URL_params[0]);
    }

    public function generateToken() {
        $token = TokenUtils::createAppRegisterToken();
        $this->addMessage("Token for registering app is:", "success");
        $this->addMessage($token);
        $this->redirect("apps");
    }

}
