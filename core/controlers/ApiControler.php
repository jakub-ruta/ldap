<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiControler
 *
 * @author jakub
 */
class ApiControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->hasView = false;
        ini_set('display_errors', 0);
        header('Content-type: application/json');
        $this->userControler = new User();
        switch (array_shift($URL_params)) {
            case 'get':
                $this->getFunction($URL_params);
                break;
            case 'set' :
                $this->setFunction($URL_params);
                break;
            case 'log' :
                $this->logFunction($URL_params);
                break;
            case 'send' :
                $this->sendFunction($URL_params);

            default :
                $this->returnNotImplemented();
                break;
        }


        return;
    }

    public function returnNotImplemented($err = "") {
        header("HTTP/1.0 404 Not Found");
        echo (json_encode(array("status" => "Not Implemented", "error" => $err)));
        return;
    }

    public function returnUnathorized($err = "") {
        header("HTTP/1.0 401 Unauthorized");
        echo (json_encode(array("status" => "Unauthorized", "error" => $err)));
        return;
    }

    public function returnSuccess() {
        header("HTTP/1.0 200");
        echo (json_encode(array("status" => "success")));
        return;
    }

    public function returnData($data, $warnings = null, $errors = null) {
        header("HTTP/1.0 200");
        echo (json_encode(array("status" => "success", "errors" => $errors, "warnings" => $warnings,
            "data" => $data)));
        return;
    }

    public function returnServerError($status, $data, $warnings = null, $errors = null) {
        header("HTTP/1.0 500 Internal Server Error");
        echo (json_encode(array("status" => $status, "errors" => $errors, "warnings" => $warnings,
            "data" => $data)));
        return;
    }

    public function getFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'userLogin':
                $this->userLogin($URL_params);
                break;
            case 'local':
                $this->getLocalFunction($URL_params);
                break;
            case 'remote':
                $this->getRemoteFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
                break;
        }
    }

    public function getLocalFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'translations' :
                $this->getLocalTranslationsFunction($URL_params);
                break;
            case 'applications' :
                $this->getLocalApplicationsFunction($URL_params);
                break;
            case 'departments' :
                $this->getLocalDepartmentsFunction($URL_params);
                break;
            case 'userlist' :
                if (!$this->userControler->getRuleValue("apps") && $this->userControler->getAdminLevel() < 4) {
                    $this->returnUnathorized();
                }
                $this->returnSuccess($this->userControler->getUserList());
                break;
            default :
                $this->returnNotImplemented();
                return;
        }
    }

    public function getRemoteFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'user':
                $this->getRemoteUserFunction($URL_params);
                break;

            default :
                $this->returnNotImplemented();
                break;
        }
    }

    public function getRemoteUserFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'rights':
                $this->getRemoteUserRights($URL_params);
                break;

            default :
                $this->returnNotImplemented();
                break;
        }
    }

    public function setFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'local' :
                $this->setLocalFunction($URL_params);
                break;
            case 'remote' :
                $this->setRemoteFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setLocalFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'PUSH' :
                $this->setLocalPUSHFunction($URL_params);
                break;
            case 'translations' :
                $this->setLocalTranslationsFunction($URL_params);
                break;
            case 'app' :
                $this->setLocalAppFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setRemoteFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'translations' :
                $this->setRemoteTranslationsFunction($URL_params);
                break;
            case 'user' :
                $this->setRemoteUserFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setRemoteUserFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'logoutEveryWhere' :
                $this->setRemoteUserLogOutEveryWhereFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function userLogin($URL_params) {
        $userToken = $_REQUEST['user_token'];
        $appToken = $_REQUEST['app_token'];
        $user_token = TokenUtils::get_instance()->getTokenByValue($userToken, "remote_login");
        if (!isset($user_token['user_internal_id'])) {
            $this->returnUnathorized("Wrong token0");
            return;
        }
        $user_token['add_data'] = json_decode($user_token['token_data'], true);
        if (TokenUtils::isTokenValid($appToken, "get", $user_token['add_data']['app_code'])) {
            $data['user'] = array_intersect_key(User::getOneUser($user_token['user_internal_id']), array_flip(array("user_internal_id", "login_name", "nickname", "mail", "admin_level", "locale", "avatar")));
            $data['user']['admin_level'] = DepartmentsUtils::gI()->getHighestRole($data['user']['user_internal_id'], $user_token['add_data']['app_code']);
            if (USINGDEPARTMENTS) {
                $data['departments'] = DepartmentsUtils::gI()->getUserDepartments($user_token['user_internal_id']);
            }
            if ($user_token['add_data']['admin_login']) {
                $data['admin_login'] = true;
                $data['admin_id'] = $user_token['add_data']['admin_id'];
            }
            $this->returnData($data);
        } else {
            $this->returnUnathorized("Wrong token1");
            return;
        }
    }

    public function setLocalTranslationsFunction($URL_params) {
        $this->userControler = new User();
        if (!$this->userControler->isUserLoggedIn()) {
            $this->returnUnathorized();
        }
        $params = array_shift($URL_params);
        if ($params == "request_update") {
            if (AppsUtils::request_update($URL_params[0]) == 0) {
                $this->returnUNotModifed();
            }
            echo (json_encode(array("status" => "success")));
            die;
        } else if ($params == "abort_update") {
            if (AppsUtils::abort_update($URL_params[0]) == 0) {
                $this->returnUNotModifed();
            }
            echo (json_encode(array("status" => "success")));
            die;
        } else if (is_numeric($params)) {
            if ($URL_params[0] == "delete") {
                $data = TranslationsUtils::deleteTranslations($params);
                if (count($data) > 0) {
                    echo (json_encode(array("status" => "success", "data" => $data)));
                } else {
                    $this->returnUnathorized();
                }
                die;
            } else {
                TranslationsUtils::update_translation($params, $_POST['new']);
                echo (json_encode(array("status" => "success")));
                die;
            }
        } else {
            $this->returnNotImplemented();
        }
    }

    public function getLocalTranslationsFunction($URL_params) {
        $params = array_shift($URL_params);
        if (is_numeric($params)) {
            $data = TranslationsUtils::getTranslation($params);
            echo (json_encode(array("status" => "success", "data" => $data)));
            die;
        } else {
            $this->returnNotImplemented();
        }
    }

    public function setRemoteTranslationsFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'missing' :
                $this->setRemoteTranslationsMissingFunction($URL_params);
                break;
            case 'request_translations' :
                $this->setRemoteTranslationsRequest_translationsFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setRemoteTranslationsMissingFunction($URL_params) {
        if (!TokenUtils::get_instance()->isTokenValid($_POST['token'], "set")) {
            $this->returnUnathorized();
            return;
        }
        $app = AppsUtils::gI()->getAppByCode($_POST['app'])['id'];
        $strings = json_decode($_POST['strings'], true);
        TranslationsUtils::addOriginals($app, $strings);
        $this->returnSuccess();
        return;
    }

    public function setLocalAppFunction($URL_params) {
        if ($URL_params[0] == "register") {
            $this->registerApp($URL_params);
            return;
        }
        if (!$this->userControler->getRuleValue("apps") && $this->userControler->getAdminLevel() < 4) {
            $this->returnUnathorized();
            return;
        }
        switch (array_shift($URL_params)) {
            case 'uppdate_staff' :
                AppsUtils::update_staff($URL_params[0], $_POST['col'], $_POST['data']);
                $this->returnSuccess();
                break;
            case 'add_staff' :
                AppsUtils::add_staff($_POST['app_id'], $_POST['user_id']);
                $this->returnSuccess();
                break;

            default :
                $this->returnNotImplemented();
        }
    }

    public function setLocalPUSHDeleteFunction($URL_params) {
        if (PushUtils::delete_device($URL_params[0], $this->userControler->getloggedUserId(),
                        ($this->userControler->getAdminLevel() > 3 ? true : false))) {
            $this->returnSuccess();
            return;
        }
        $this->returnUnathorized();
    }

    public function setLocalPUSHFunction($URL_params) {
        if (!$this->userControler->isUserLoggedIn()) {
            $this->returnUnathorized();
            return;
        }
        switch (array_shift($URL_params)) {
            case 'new' :
                PushUtils::storePlayerId($_POST['player_id'], $this->userControler->getloggedUserId());
                $this->returnSuccess();
                break;
            case 'delete_device' :
                $this->setLocalPUSHDeleteFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function sendPUSHFunction($URL_params) {
        if (TokenUtils::verifyPushToken($_POST['token'])) {
            PushUtils::sendNotification($_POST['push_title'], $_POST['push_message'], array($_POST['push_user_id']));
        } else {
            $this->returnUnathorized();
        }
    }

    public function sendFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'PUSH' :
                $this->sendPUSHFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function logFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'PUSH' :
                $this->logPUSHFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function logPUSHFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'webhooks' :
                PushUtils::storeWebHooks(json_encode(array("post" => $_POST, "body" => file_get_contents("php://input"))));
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function getRemoteUserRights($URL_params) {
        if (!isset($URL_params[0]) && !isset($URL_params[1])) {
            $this->returnNotImplemented("missing params");
            return;
        }
        if (!DepartmentsUtils::gI()->canUserEditUser(User::getUserId(), $URL_params[1], "rights")) {
            $this->returnUnathorized();
            return;
        }
        $app = AppsUtils::gI()->getAppByCode(str_replace("app_", "", $URL_params[0]));
        $rights = DepartmentsUtils::gI()->getRightsForUser(User::getUserId(), $URL_params[1]);
        $main_role = DepartmentsUtils::gI()->getHighestRole($URL_params[1], $app['app_code']);
        if (array_key_exists($URL_params[0], $rights)) {
            $respon_staff = StringUtils::useOutputBufferr("ResponsibleStaff_stringFactory", array("users" => AppsUtils::gI()->getResponsibleStaffByAppId($app['id'])));
            $data = RightsUtils::gI()->getRemoteRights($app, $URL_params[1]);
            $this->returnData(array(
                "position" => DepartmentsUtils::gI()->roles[$main_role],
                "respon_staff" => $respon_staff,
                "html" =>
                StringUtils::useOutputBufferr("rights_factory",
                        array("rights" => $data, "only_granteable" => (User::getInstance()->getAdminLevel() > 4 ? false : true)))));
            return;
        }
        $this->returnUnathorized();
    }

    public function getLocalApplicationsFunction($URL_params) {
        if (!TokenUtils::get_instance()->isTokenValid($_POST['token'], "get")) {
            $this->returnUnathorized();
            return;
        }
        $user = User::getOneUser($_POST['user_id']);
        $rights = ArrayUtils::makeKeyArray(User::getUserRights($_POST['user_id'], "access"), "name");
        $apps = AppsUtils::getAllApps();
        foreach ($apps as $key => $app) {
            if ($user['admin_level'] < 4 &&
                    ($rights["app_" . $app['app_code']]['value'] == 0 || $app['disabled'] == 1)) {
                unset($apps[$key]);
            } else {
                $data[] = array_intersect_key($app, array_flip(array("name", "app_code", "url", "group_name", "group_code")));
            }
        }

        $data = ArrayUtils::makeKeyCategoryArray($data, "group_code");
        $this->returnData($data);
        return;
    }

    public function setRemoteUserLogOutEveryWhereFunction($URL_params) {
        if (!TokenUtils::get_instance()->isTokenValid($_POST['token'], "set")) {
            $this->returnUnathorized();
            return;
        }
        $apps = AppsUtils::gI()->getAllApps();
        $rights = ArrayUtils::makeKeyArray(User::getInstance()->getUserRights($_POST['user_id'], "access"), "name");
        foreach ($apps as $app) {
            if (!$app['disabled'] && !$app['external']) {
                if ($rights['app_' . $app['app_code']]['value']) {
                    $data[$app['app_code']] = CURLUtils::getInstance()->makeReqest("https://" . $app['adress'] . "/ajax/set/local/user/logout",
                            array("token" => TokenUtils::get_instance()->getRemoteToken($app['app_code'], "set"),
                                "user_portal_id" => $_POST['user_id']));
                }
            }
        }
        SessionsUtils::get_instance()->invalidAllSessionsByUserId($_POST['user_id']);
        $this->returnData(null, json_encode($data));
    }

    public function setRemoteTranslationsRequest_translationsFunction($URL_params) {
        if (!TokenUtils::get_instance()->isTokenValid($_POST['token'], "set")) {
            $this->returnUnathorized();
            return;
        }
        AppsUtils::gI()->requestTranslationsUpdate($_POST['app_name']);
        $this->returnSuccess();
    }

    public function getLocalDepartmentsFunction($URL_params) {
        if (!TokenUtils::get_instance()->isTokenValid($_POST['token'], "get")) {
            $this->returnUnathorized();
            return;
        }
        $data = DepartmentsUtils::gI()->getAll();
        $this->returnData($data);
    }

    public function registerApp($URL_params) {
        if (!TokenUtils::get_instance()->isLocalTokenValid($_POST['token'])) {
            $this->returnUnathorized("Wrong token");
        }
        $data = array("name" => $_POST['app_code'],
            "app_code" => $_POST['app_code'],
            "adress" => $_POST['adress'],
            "url" => "https://" . $_POST['adress'],
            "has_translations" => 1,
            "using_departments" => $_POST['using_departments']);
        AppsUtils::gI()->createApp($data);
        $token = StringUtils::generate_string(238);
        $this->cretaInternalTokens($token, $_POST['app_code']);
        $this->returnData(array("token" => $token));
    }

    public function cretaInternalTokens($token, $app_code) {
        $data = array(
            array("function" => "get",
                "app_code" => $app_code,
                "token" => $token,
                "support_auto_update" => 1),
            array("function" => "set",
                "app_code" => $app_code,
                "token" => $token,
                "support_auto_update" => 1),
            array("function" => "send",
                "app_code" => $app_code,
                "token" => $token,
                "support_auto_update" => 1),
            array("function" => "setup",
                "app_code" => $app_code,
                "token" => $token,
                "support_auto_update" => 0));
        TokenUtils::get_instance()->createInternalTokens($data);
    }

}
