<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuBuilder
 *
 * @author jakub
 */
class MenuBuilder {

    public static function build_menu() {

        $mb = MenuBuilder_new::getInstance();

        if (User::get_instance()->getRuleValue("apps", "view")) {
            $mb->addToMenu("Apps", "Apps", "apps", false, "fas fa-list-alt");
            $mb->addToMenu("Apps", "Langs", "langs", false, "fas fa-globe-europe");
        }
        if (DepartmentsUtils::gI()->isUserAdminAnywhere()) {
            $mb->addToMenu("Departements admin", "Users", "departments-users", false, "fas fa-users");
        }
        if (DepartmentsUtils::gI()->isUserAdminAnywhere(2)) {
            $mb->addToMenu("Departements admin", "Departments", "departments", false, "fas fa-toolbox");
            $mb->addToMenu("Apps", "Translations", "translations", false, "fas fa-language");
        }
    }

    //put your code here
}
