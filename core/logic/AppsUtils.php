<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppsUtils
 *
 * @author jakub
 */
class AppsUtils {

    protected static $registrationPath = "/ajax/set/local/user/register";
    protected static $disablePath = "/ajax/set/local/user/disable";
    public $editable = array("name", "under_development", "adress", "url",
        "disabled");
    protected static $_instance;

    /**
     *
     * @var class Mysqli
     */
    protected $db;

    /**
     *
     * @return AppsUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new AppsUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return AppsUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public static function getAllApps($user_id = null) {
//$apps = Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE 1 ORDER BY order_key ASC');
        $apps = MysqliDb::getInstance()
                ->join("applications_in_groups r", "l.id=r.app_id", "LEFT")
                ->join("applications_groups r2", "r.group_id=r2.group_id", "LEFT")
                ->orderBy("l.id", "ASC")
                ->get("applications l");
        if ($user_id == null)
            return $apps;
        foreach ($apps as $key => $app) {
            $apps[$key]['responsible_staff'] = self::getResponsibleStaffByAppId($app['id']);
            if (!$app['external'] && $app['has_translations']) {
                if (self::can_user_edit_translations($user_id, $app['id'])) {
                    $apps[$key]['can_user_edit_translations'] = 1;
                } else {
                    $apps[$key]['can_user_edit_translations'] = 0;
                }
            }
        }
        return $apps;
    }

    public static function getResponsibleStaffByAppId($app) {
        return MysqliDb::getInstance()
                        ->join("users_personal_informations r", "r.user_internal_id=l.user_id")
                        ->where("l.app_id", $app)
                        ->get("applications_responsible_staff l", null, "r.user_internal_id, r.login_name, r.nickname, r.mail, r.avatar, l.visible, l.position, l.translations, r.disabled_account AS disabled");
        /* return Db::queryAll('SELECT ' . TABLEPREFIX . 'applications_responsible_staff.position, '
          . '' . TABLEPREFIX . 'applications_responsible_staff.visible, '
          . '' . TABLEPREFIX . 'applications_responsible_staff.translations, '
          . '' . TABLEPREFIX . 'users_personal_informations.login_name, '
          . '' . TABLEPREFIX . 'users_personal_informations.nickname, '
          . '' . TABLEPREFIX . 'users_personal_informations.mail '
          . 'FROM ' . TABLEPREFIX . 'applications_responsible_staff '
          . 'LEFT JOIN ' . TABLEPREFIX . 'users_personal_informations ON '
          . '' . TABLEPREFIX . 'applications_responsible_staff.user_id=' . TABLEPREFIX . 'users_personal_informations.user_id '
          . 'WHERE ' . TABLEPREFIX . 'applications_responsible_staff.app_id=?',
          $app);
         */
    }

    public static function getAppByRule($rule) {
        return MysqliDb::getInstance()->where("rule_name", $rule)
                        ->getOne("applications");
        //return Db::queryOne('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE rule_name=?', $rule);
    }

    public static function getAppById($id) {
        return MysqliDb::getInstance()->where("id", $id)
                        ->getOne("applications");
        return Db::queryOne('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE id=?', $id);
    }

    /* public static function getAppByCode($name) {
      return MysqliDb::getInstance()->where("name", $name)
      ->getOne("applications");
      return Db::queryOne('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE app_code=?', $name);
      } */

    public static function getAppRuleByName($appName) {
        return MysqliDb::getInstance()->where("name", htmlspecialchars_decode($appName))
                        ->getValue("applications", "rule_name");
        return Db::querySingle('SELECT rule_name FROM ' . TABLEPREFIX . 'applications WHERE name=? ',
                        htmlspecialchars_decode($appName));
    }

    public static function getAppAdressByName($URL_params) {
        return $this->getAppByCode(htmlspecialchars_decode($URL_params))['adress'];
        return Db::querySingle('SELECT adress FROM ' . TABLEPREFIX . 'applications WHERE name=? ',
                        htmlspecialchars_decode($URL_params));
    }

    public static function getAllAppsForTranslations($id) {
        $apps = MysqliDb::getInstance()->
                join("applications_responsible_staff r", "l.id=r.app_id")
                ->join("applications_in_groups r2", "l.id=r2.app_id")
                ->join("applications_groups r3", "r2.group_id=r3.group_id")
                ->where("r.user_id", $id)
                ->get("applications l", null, "l.id AS app_id, l.*, r.*, r2.*, r3.*");
        foreach ($apps as $key => $app) {
            $apps[$key]['responsible_staff'] = self::getResponsibleStaffByAppId($app['id']);
            if (!$app['external'] && $app['has_translations']) {
                if (self::can_user_edit_translations($id, $app['id'])) {
                    $apps[$key]['can_user_edit_translations'] = 1;
                } else {
                    $apps[$key]['can_user_edit_translations'] = 0;
                }
            }
        }
        return $apps;
        $apps = Db::queryAll('SELECT *,' . TABLEPREFIX . 'applications.id AS app_id  FROM ' . TABLEPREFIX . 'applications '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'applications_responsible_staff ON ('
                        . TABLEPREFIX . 'applications_responsible_staff.app_id=' . TABLEPREFIX . 'applications.id AND '
                        . TABLEPREFIX . 'applications_responsible_staff.user_id=?) '
                        . ' WHERE external=0 ORDER BY order_key ASC', $id);

        return $apps;
    }

    public static function can_user_edit_translations($user_id, $app_id) {

        $can = Db::querySingle('SELECT translations FROM ' . TABLEPREFIX . 'applications_responsible_staff'
                        . ' WHERE user_id=? AND app_id=? ',
                        $user_id, $app_id);
        if (isset($can)) {
            return $can;
        } else {
            return false;
        }
    }

    public static function request_update($URL_params) {
        if (!self::getAppById($URL_params)['has_translations'])
            throw new Exception("This app do not have translations");

        return Db::query('UPDATE ' . TABLEPREFIX . 'applications SET'
                        . ' translation_update_request=?, translations_update_request_time=?'
                        . ' WHERE id=?', 1, time(), $URL_params);
    }

    public static function abort_update($URL_params) {
        if (!self::getAppById($URL_params)['has_translations'])
            throw new Exception("This app do not have translations");
        return Db::query('UPDATE ' . TABLEPREFIX . 'applications SET'
                        . ' translation_update_request=?, translations_update_request_time=?'
                        . ' WHERE id=?', 0, time(), $URL_params);
    }

    public static function getAppsWithUpdateRequest() {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE has_translations=? '
                        . 'AND translation_update_request=? AND translations_update_request_time<?',
                        1, 1, time() + 60 * 60);
    }

    public static function setUpdateReqest($app_id) {
        return Db::query('UPDATE ' . TABLEPREFIX . 'applications SET'
                        . ' translation_update_request=?, trasnlations_last_upload=? '
                        . ' WHERE id=?', 0, date("Y-m-d H:i:s"), $app_id);
    }

    public static function getResponsibleStaffByAppIdAll($app) {
        $db = MysqliDb::getInstance();
        $db->join("users_personal_informations r", "l.user_id=r.user_internal_id");
        $db->where("app_id", $app);
        return $db->get("applications_responsible_staff l", null, "l.*, r.login_name, r.mail, r.nickname,"
                        . " r.admin_level, r.disabled_account, r.avatar");
    }

    public static function update_staff($id, $col, $data) {
        $array = array("position", "visible", "translations", "in_app_admin_level");
        if (!in_array($col, $array)) {
            throw new Exception("This operation is not allowed!");
        }
        $db = MysqliDb::getInstance();
        $db->where("respon_id", $id);
        $db->update("applications_responsible_staff", array($col => $data), 1);
        echo(json_encode($db->getLastQuery()));
        die;
    }

    public static function getAdminLevelByResponsibleStaffByAppNameUserId($user_id, $app_name) {
        $db = MysqliDb::getInstance();
        $db->where("name", $app_name);
        $app_id = $db->getValue("applications", "id");
        $db->where("app_id", $app_id);
        $db->where("user_id", $user_id);
        return $db->getValue("applications_responsible_staff", "in_app_admin_level");
    }

    public static function add_staff($app_id, $user_id) {
        $db = MysqliDb::getInstance();
        $db->insert("applications_responsible_staff", array("app_id" => $app_id, "user_id" => $user_id));

        return true;
    }

    public function getAppByCode($code, $disabled = false) {
        if ($disabled == false) {
            $this->db->where("disabled", 0);
        }
        return $this->db->where("app_code", $code)
                        ->getOne("applications");
    }

    public function registerUserInApp($user_id, $app_code) {
        $app = $this->getAppByCode($app_code, true);
        if ($app['external']) {
            throw new RemoteRigistrationException("This app is using diferent authentification system. You have to contact app´s responsible staff to register user and assign rights. If possible create request via support ticket.");
        }
        $user['user'] = User::getOneUser($user_id);
        if (USINGDEPARTMENTS && $app['using_departments']) {
            $user['departments'] = DepartmentsUtils::gI()->getUserDepartments($user_id);
        }
        $send_data = array("token" => TokenUtils::getRemoteToken($app_code, "set"),
            "action" => "register_user",
            "user" => $user);
        $data = CURLUtils::getInstance()->makeReqest($app['url'] . self::$registrationPath, $send_data);
        $data = json_decode($data, true);
        if ($data['status'] != "success") {
            throw new RemoteRigistrationException("User registration proccess: Something went wrong - " . $data['status']);
        }
        return true;
    }

    public function disableUserInApp($user_id, $app_code) {
        $app = $this->getAppByCode($app_code, true);
        if ($app['external']) {
            throw new RemoteRigistrationException("This app is using diferent authentification system. You have to contact app´s responsible staff to register user and assign rights. If possible create request via support ticket.");
        }
        $user['user'] = User::getOneUser($user_id);
        $send_data = array("token" => TokenUtils::getRemoteToken($app_code, "set"),
            "action" => "disable_user",
            "user" => $user);
        $data = CURLUtils::getInstance()->makeReqest($app['url'] . self::$disablePath, $send_data);
        $data = json_decode($data, true);
        if ($data['status'] != "success") {
            throw new RemoteRigistrationException("User disabling proccess: Something went wrong - " . $data['status']);
        }
        return true;
    }

    public function getRemoteRights($app_code, $user_portal_id, $app) {
        return json_decode(
                CURLUtils::getInstance()->makeReqest($app['url']
                        . "/ajax/get/local/user/rights",
                        array("user_portal_id" => $user_portal_id[1],
                            "token" => TokenUtils::getRemoteToken(
                                    str_replace("app_", "", $app_code[0]
                                    ),
                                    "get"))), true);
    }

    public function delete_staff($app_id, $respon_id) {
        $this->db->where("respon_id", $respon_id)->delete("applications_responsible_staff", 1);
    }

    public function getAppGroup($app_id) {
        return $this->db->where("l.app_id", $app_id)
                        ->join("applications_groups r", "l.group_id=r.group_id")
                        ->get("applications_in_groups l");
    }

    public function getAllGroups() {
        return $this->db->get("applications_groups");
    }

    public function changeAppGroup($app_id, $group_id) {
        $data = array("app_id" => $app_id, "group_id" => $group_id);
        $updateColumns = Array("group_id");
        $lastInsertId = "log_id";
        $this->db->onDuplicate($updateColumns, $lastInsertId);
        $id = $this->db->insert('applications_in_groups', $data);
    }

    public function updateApp($app_id, $data) {
        $this->db->where("id", $app_id)
                ->update("applications", array_intersect_key($data, array_flip($this->editable)));
    }

    public function requestTranslationsUpdate($app_code) {
        $this->db->where("app_code", $app_code)
                ->update("applications", array("translation_update_request" => 1,
                    "translations_update_request_time" => time()));
    }

    public function createApp($data) {
        $this->db->insert("applications", $data);
    }

}
