<?php

$start = time();


//debuger

require_once '../data/core/debuger/tracy.php';

use Tracy\Debugger;

Debugger::enable(Debugger::DETECT, __DIR__ . '/app_data/log');

Debugger::timer('gen_time_all');


ini_set('max_execution_time', 20);
// Konstanty a nastavení aplikace

require_once 'app_data/settings.php';

$settings = new settings();
require_once 'core/logic/Db.php';
Db::connect($settings->getDb_server(), $settings->getDb_name(), $settings->getDb_user(), $settings->getDb_password());

require_once '../data/core/' . MASTERFOLDER . "/app.php";
