<?php

ini_set('display_errors', 0);
//error_reporting(0);
ini_set('max_execution_time', 20);
session_start();
// Konstanty a nastavení aplikace
if ($_SERVER['SERVER_NAME'] == "ldap.topescape.local") {
    require_once 'core/app_data/sett_dev.php';
} else {
    require_once 'core/app_data/settings.php';
}
$settings = new settings();
//debuger
require_once '../data/core/debuger/src/tracy.php';

use Tracy\Debugger;

if ($_SESSION['user']['admin_level'] > 3 || ALWAYSSHOWDEBUG) {
    Debugger::enable(Debugger::DEVELOPMENT);
} else {
    //Debugger::enable();
}



// Nastavení interního kódování pro funkce pro práci s řetězci
mb_internal_encoding("UTF-8");

// Callback pro automatické načítání tříd controllerů a modelů
function autoloadClass($class) {

    if (preg_match('/Controler$/', $class)) {
        require("core/controlers/" . $class . ".php");
    } else {
        require("core/logic/" . $class . ".php");
    }
}

//registrace autoloaderu
spl_autoload_register("autoloadClass");



// Připojení k databázi
Db::connect($settings->getDb_server(), $settings->getDb_name(), $settings->getDb_user(), $settings->getDb_password());
$db = new MysqliDb($settings->getDb_server(), $settings->getDb_user(),
        $settings->getDb_password(), $settings->getDb_name());
$db->setPrefix(TABLEPREFIX);
$db->setTrace(ENABLELOGQUERIES, TABLEPREFIX);
//vytvření logeru session
// Vytvoření routeru a zpracování parametrů od uživatele z URL
$router = new RouterControler();
$router->setSession_logger_class(new SessionLoggerCollectorClass());
$router->execute(array($_SERVER['REQUEST_URI']));


// Vyrenderování šablony
$router->renderView();
unset($router->session_logger_class);
