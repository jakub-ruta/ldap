<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminControler
 *
 * @author jakub
 */
class MailControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->userControler = new User();

        if (!$this->userControler->isUserLoggedIn()) {
            $this->redirect("login/?redirect=" . $_SERVER['REQUEST_URI']);
        }
        if ($this->userControler->getRuleValue("mail", 0) == 0) {
            $this->redirectToError("You do not have right.", 401);
        }
        $this->data['user'] = $this->userControler->getLoggedUser();

        $this->view = "mail";
    }

}
