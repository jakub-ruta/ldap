<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AjaxControler
 *
 * @author jakub
 */
class AjaxControler extends Controler {

//put your code here
    public function execute($URL_params) {
        header('Content-type: application/json');
        $this->userControler = new User();
        switch (array_shift($URL_params)) {
            case 'get' :
                $this->getFunction($URL_params);
                break;
            case 'set' :
                $this->setFunction($URL_params);
                break;
            case 'log' :
                $this->logFunction($URL_params);
                break;
            case 'send' :
                $this->sendFunction($URL_params);

                break;
            default :
                $this->returnNotImplemented();
        }
//ajax/get/userLogin    (POST = app_token, user_token)
//echo (json_encode(array("status" => "Not Implemented", "data" => array(0 => array("est", 5, "tooltip1", "green"), 1 => array("test", 4, "tooltip2", "lime")))));
        die;
    }

    public function getFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'remote' :
                $this->getRemoteFunction($URL_params);
                break;
            case 'userLogin' :
                $this->getUserLoginFunction();
                break;
            case 'local':
                $this->getLocalFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'local' :
                $this->setLocalFunction($URL_params);
                break;
            case 'remote' :
                $this->setRemoteFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setLocalFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'PUSH' :
                $this->setLocalPUSHFunction($URL_params);
                break;
            case 'translations' :
                $this->setLocalTranslationsFunction($URL_params);
                break;
            case 'app' :
                $this->setLocalAppFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setRemoteFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'translations' :
                $this->setRemoteTranslationsFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function getLocalFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'translations' :
                $this->getLocalTranslationsFunction($URL_params);
                break;
            case 'applications' :
                $this->getLocalApplicationsFunction($URL_params);
                break;
            case 'userlist' :
                if (!$this->userControler->getRuleValue("apps") && $this->userControler->getAdminLevel() < 4) {
                    $this->returnUnathorized();
                }
                $this->returnSuccess($this->userControler->getUserList());
                break;

            default :
                $this->returnNotImplemented();
                return;
        }
    }

    public function sendFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'PUSH' :
                $this->sendPUSHFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function logFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'PUSH' :
                $this->logPUSHFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function logPUSHFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'webhooks' :
                PushUtils::storeWebHooks(json_encode(array("post" => $_POST, "body" => file_get_contents("php://input"))));
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function returnNotImplemented() {
        header("HTTP/1.0 404 Not Found");
        echo (json_encode(array("status" => "Not Implemented")));
        die;
    }

    public function returnUnathorized() {
        header("HTTP/1.0 401 Unauthorized");
        echo (json_encode(array("status" => "Unauthorized")));
        die;
    }

    public function returnSuccess($data = null) {
        header("HTTP/1.0 200");
        echo (json_encode(array("status" => "success", "data" => $data)));
        die;
    }

    public function returnUNotModifed() {
        header("HTTP/1.0 304 Not Modifed");
        echo (json_encode(array("status" => "Not changed")));
        die;
    }

    public function returnRemoteNotResponding() {
        echo (json_encode(array("status" => "Remote not responding")));
        die;
    }

    public function getRemoteFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'data' :
                $this->getRemoteDataFunction($URL_params);
                break;
            case 'rights' :
                $this->getRemoteRightsFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function getRemoteDataFunction($URL_params) {

        $app = AppsUtils::getAppById($URL_params[0]);
        if (!$this->userControler->getRuleValue($app['rule_name']) && $this->userControler->getAdminLevel() < 4) {
            $this->returnUnathorized();
        }
        if ($app['disabled'] == 1) {
            echo (json_encode(array("status" => "Disabled app")));
            die;
        }
        $remote = json_decode($this->getFromURL("https://" . $app['adress'] .
                        "/ajax/get/local/data/"
                        , array("token" => TokenUtils::getRemoteToken($app['adress']),
                    "user_id" => $this->userControler->getloggedUserId())), true);
        if (!isset($remote['status'])) {
            $this->returnRemoteNotResponding();
        }
        echo (json_encode($remote));
        die;
    }

    public function getRemoteRightsFunction($URL_params) {
        if (!$this->userControler->getRuleValue($URL_params[0]) && $this->userControler->getAdminLevel() < 4) {
            $this->returnUnathorized();
        }
        $app = AppsUtils::getAppByRule($URL_params[0]);
        if ($app['disabled'] == 1) {
            echo (json_encode(array("status" => "Disabled app")));
            die;
        }
        $remote = json_decode($this->getFromURL("https://" . $app['adress'] .
                        "/ajax/get/local/rights/"
                        , array("token" => TokenUtils::getRemoteToken($app['adress']),
                    "user_id" => $this->userControler->getloggedUserId())), true);
        if (!isset($remote['status'])) {
            $this->returnRemoteNotResponding();
        }
        echo (json_encode($remote));
        die;
    }

    protected function getFromURL($url, $post) {
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handler, CURLOPT_POSTFIELDS, http_build_query($post));
        $obsah = curl_exec($handler);
        curl_close($handler);
        return $obsah;
    }

//ajax/get/userLogin    (POST = app_token, user_token)
    public function getUserLoginFunction() {
        if (TokenUtils::remoteTokenValid("get", $_REQUEST['app_token'])) {
            $user_token = TokenUtils::getUserToken($_REQUEST['user_token']);
            $data = $this->userControler->getUserById($user_token['user_id']);
            if ($data['admin_level'] < 3) {
                $data['admin_level'] = AppsUtils::getAdminLevelByResponsibleStaffByAppNameUserId($data['user_id'], $user_token['app']);
            }
            echo(json_encode(array("status" => "success",
                "data" => $data)));
        } else {
            header("HTTP/1.0 404 Not Found");
            echo (json_encode(array("status" => "Wrong token")));
        }
        ActionLogUtils::logTest(json_encode(array($_POST, $data)));
        die;
    }

    public function setLocalPUSHFunction($URL_params) {
        if (!$this->userControler->isUserLoggedIn()) {
            $this->returnUnathorized();
        }
        switch (array_shift($URL_params)) {
            case 'new' :
                PushUtils::storePlayerId($_POST['player_id'], $this->userControler->getloggedUserId());
                $this->returnSuccess();
                break;
            case 'delete_device' :
                $this->setLocalPUSHDeleteFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function sendPUSHFunction($URL_params) {
        if (TokenUtils::verifyPushToken($_POST['token'])) {
            PushUtils::sendNotification($_POST['push_title'], $_POST['push_message'], array($_POST['push_user_id']));
        } else {
            $this->returnUnathorized();
        }
    }

    public function setLocalTranslationsFunction($URL_params) {
        if (!$this->userControler->isUserLoggedIn()) {
            $this->returnUnathorized();
        }
        $params = array_shift($URL_params);
        if ($params == "request_update") {
            if (AppsUtils::request_update($URL_params[0]) == 0) {
                $this->returnUNotModifed();
            }
            echo (json_encode(array("status" => "success")));
            die;
        } else if ($params == "abort_update") {
            if (AppsUtils::abort_update($URL_params[0]) == 0) {
                $this->returnUNotModifed();
            }
            echo (json_encode(array("status" => "success")));
            die;
        } else if (is_numeric($params)) {
            if ($URL_params[0] == "delete") {
                $data = TranslationsUtils::deleteTranslations($params);
                if (count($data) > 0) {
                    echo (json_encode(array("status" => "success", "data" => $data)));
                } else {
                    $this->returnUnathorized();
                }
                die;
            } else {
                if (TranslationsUtils::update_translation($params, $_POST['new']) == 0) {
                    $this->returnUNotModifed();
                }

                echo (json_encode(array("status" => "success")));
                die;
            }
        } else {
            $this->returnNotImplemented();
        }
    }

    public function getLocalTranslationsFunction($URL_params) {
        $params = array_shift($URL_params);
        if (is_numeric($params)) {
            $data = TranslationsUtils::getTranslation($params);
            echo (json_encode(array("status" => "success", "data" => $data)));
            die;
        } else {
            $this->returnNotImplemented();
        }
    }

    public function setRemoteTranslationsFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'missing' :
                $this->setRemoteTranslationsMissingFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setRemoteTranslationsMissingFunction($URL_params) {
        if (!TokenUtils::verifySetToken($_POST['token'])) {
            $this->returnUnathorized();
        }
        $app = AppsUtils::getAppByCode($_POST['app'])['id'];
        $strings = json_decode($_POST['strings'], true);
        TranslationsUtils::addOriginals($app, $strings);
        $this->returnSuccess();
    }

    public function setLocalPUSHDeleteFunction($URL_params) {
        if (PushUtils::delete_device($URL_params[0], $this->userControler->getloggedUserId(),
                        ($this->userControler->getAdminLevel() > 3 ? true : false))) {
            $this->returnSuccess();
        }
        $this->returnUnathorized();
    }

    public function setLocalAppFunction($URL_params) {
        if (!$this->userControler->getRuleValue("apps") && $this->userControler->getAdminLevel() < 4) {
            $this->returnUnathorized();
        }
        switch (array_shift($URL_params)) {
            case 'uppdate_staff' :
                AppsUtils::update_staff($URL_params[0], $_POST['col'], $_POST['data']);
                $this->returnSuccess();
                break;
            case 'add_staff' :
                AppsUtils::add_staff($_POST['app_id'], $_POST['user_id']);
                $this->returnSuccess();
                break;

            default :
                $this->returnNotImplemented();
        }
    }

    public function getLocalApplicationsFunction($URL_params) {
        if (!TokenUtils::verifyGetToken($_POST['token'])) {
            $this->returnUnathorized();
            return;
        }
        $rights = User::getUserRights($_POST['user_id']);
        foreach ($rights as $key => $value) {
            if ($value['value'] == 1) {
                $data[] = AppsUtils::getAppByRule($value['name']);
            }
        }
        foreach ($data as $key => $value) {
            if ($value != null) {
                $app[] = array("name" => $value['name'], "href" => $value['url']);
            }
        }
        $this->returnSuccess($app);
    }

}
