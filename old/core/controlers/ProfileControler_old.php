<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProfileControler
 *
 * @author jakub
 */
class ProfileControler extends Controler {

    //put your code here
    public function execute($parametry) {
        $this->userControler = new User();

        if (!$this->userControler->isUserLoggedIn()) {
            $this->redirect("login");
        }
        $this->data['user'] = $this->userControler->getLoggedUser();
        $this->data['user_edit'] = $this->userControler->getOneUsers($this->userControler->getloggedUserId());
        $this->data['user_devices'] = PushUtils::getDevicesByUserid($this->userControler->getloggedUserId());
        $this->view = "profile";
        bdump($this->data['user']);
        $this->data['can_change_profile'] = $this->userControler->getRuleValue("change_profile", 0);
        $this->data['user_editable_fields'] = array("email", "name", "surname", "nickname", "birth_date", "mobile");
        if (isset($_POST['password'])) {
            if ($_POST['password'] == $_POST['password_again']) {
                $this->userControler->setNewPassword($_POST['password']);
                $this->addMessage("Password has been updated.", "success");
            } else {
                $this->addMessage("Password has not been updated", "danger");
                $this->addMessage("Password are not same, try it again.", "warning");
            }
        } else if (isset($_POST['nickname'])) {
            if ($this->userControler->getRuleValue("change_profile", 0) == 0) {
                $this->redirectToError("You do not have right to update profile.", 401);
            }
            $data = array_intersect_key($_POST,
                    array_flip($this->data['user_editable_fields']));
            $this->userControler->updateProfile($data, $this->userControler->getloggedUserId());
            $this->addMessage("Profile has been changed", "success");
            $this->redirect("profile");
        }
    }

}
