<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class DashBoardControler extends Controler {

    public function execute($URL_params) {
        $this->userControler = new User();

        if (!$this->userControler->isUserLoggedIn()) {
            $this->redirect("login");
        }
        $this->data['user'] = $this->userControler->getLoggedUser();
        $this->view = "dashBoard";
        $apps = AppsUtils::getAllApps($this->userControler->getloggedUserId());

        foreach ($apps as $key => $app) {
            if ($this->userControler->getAdminLevel() < 4 &&
                    ($this->userControler->getRuleValue($app['rule_name']) == 0 || $app['disabled'] == 1)) {
                unset($apps[$key]);
            }
            if ($this->userControler->getAdminLevel() > 3 && !$app['external'] && $app['has_translations']) {
                $apps[$key]['can_user_edit_translations'] = 1;
            }
        }
        $this->data['apps'] = $apps;
    }

}
