<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class AppsControler extends Controler {

    public function execute($URL_params) {
        $this->userControler = new User();

        if (!$this->userControler->isUserLoggedIn()) {
            $this->redirect("login");
        }

        if ($this->userControler->getRuleValue("apps", 0) == 0) {
            $this->redirectToError("You do not have right.", 401);
        }
        $this->data['user'] = $this->userControler->getLoggedUser();
        if (isset($URL_params[0])) {
            $this->app($URL_params);
        } else {
            $this->view = "apps";
            $this->data['apps'] = AppsUtils::getAllApps($this->userControler->getloggedUserId());
        }
    }

    public function app($URL_params) {
        $this->data['app_editable_fields'] = array("name", "external", "adress", "url", "under_development",
            "disabled", "rule_name", "order_key", "has_translations");
        $this->data['app'] = AppsUtils::getAppById($URL_params[0]);
        $this->data['responsible_staff'] = AppsUtils::getResponsibleStaffByAppIdAll($URL_params[0]);
        $this->data['langs'] = TranslationsUtils::getAppLangsByAppId($URL_params[0]);
        $this->view = "app";
    }

}
