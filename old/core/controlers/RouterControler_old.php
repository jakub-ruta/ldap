<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Router
 *
 * @author jakub
 */
class RouterControler extends Controler {

    // Instance controlleru
    protected $controler;

    // Metoda převede pomlčkovou variantu controlleru na název třídy
    private function toCamleCase($text) {
        $sentence = str_replace('-', ' ', $text);
        $sentence = ucwords($sentence);
        $sentence = str_replace(' ', '', $sentence);
        return $sentence;
    }

    // Naparsuje URL adresu podle lomítek a vrátí pole parametrů
    private function parseURL($url) {
        // Naparsuje jednotlivé části URL adresy do asociativního pole
        $naparsovanaURL = parse_url($url);
        // Odstranění počátečního lomítka
        $naparsovanaURL["path"] = ltrim($naparsovanaURL["path"], "/");
        // Odstranění bílých znaků kolem adresy
        $naparsovanaURL["path"] = trim($naparsovanaURL["path"]);
        // Rozbití řetězce podle lomítek
        $rozdelenaCesta = explode("/", $naparsovanaURL["path"]);
        return $rozdelenaCesta;
    }

    // Naparsování URL adresy a vytvoření příslušného controlleru
    public function execute($parametry) {

        $parsedURL = $this->parseURL($parametry[0]);
        if (empty($parsedURL[0])) {
            $this->redirect("login");
        } else if ($parsedURL[0] != "error" && SpravceIP::isIPBanned()) {
            $this->redirectToError('error', 423);
        } else {
            // kontroler je 1. parametr URL
            $controlerClass = $this->toCamleCase(array_shift($parsedURL)) . 'Controler';
        }

        if (file_exists('core/controlers/' . $controlerClass . '.php')) {
            $this->controler = new $controlerClass;
        } else {
            $this->redirectToError('error', 404);
        }

        // Volání controlleru
        $this->controler->execute($parsedURL);

        // Nastavení proměnných pro šablonu
        $this->data = $this->controler->data;
        $this->data['title'] = $this->controler->hlavicka['title'];


        // Nastavení hlavní šablony
        if ($this->controler->defaultTemplate) {
            $this->view = "template";
        } else {
            $this->view = $this->controler->Template;
        }
    }

}
