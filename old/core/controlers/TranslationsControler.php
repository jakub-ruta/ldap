<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class TranslationsControler extends Controler {

    public function execute($URL_params) {
        $this->userControler = new User();

        if (!$this->userControler->isUserLoggedIn()) {
            $this->redirect("login");
        }
        $this->data['user'] = $this->userControler->getLoggedUser();
        if (isset($URL_params[0])) {
            $this->translation($URL_params);
        } else {
            $this->translations();
        }
    }

    protected function translations() {
        $apps = AppsUtils::getAllAppsForTranslations($this->userControler->getloggedUserId());
        foreach ($apps as $key => $app) {
            if (($this->userControler->getAdminLevel() < 4 &&
                    $app['translations'] != 1) || $app['has_translations'] == 0) {
                unset($apps[$key]);
            }
        }
        if (count($apps) == 0) {
            $this->redirectToError("You do not have any app where you can edit translations!", 403);
        }
        $this->data['apps'] = $apps;
        $this->view = "translations";
    }

    public function translation($URL_params) {
        $app = AppsUtils::getAppById($URL_params[0]);
        if ($app['has_translations'] && ($this->userControler->getAdminLevel() > 3 ||
                AppsUtils::can_user_edit_translations($this->userControler->getloggedUserId(), $URL_params[0]))) {
            $this->view = "translation";
            $this->data['app'] = $app;
            $this->data['supported_lang'] = TranslationsUtils::getSupportedLangByAppId($URL_params[0]);
            if (isset($URL_params[1])) {
                foreach ($this->data['supported_lang'] as $lang) {
                    if ($lang['lang_code'] == $URL_params[1]) {
                        $lang_id = $lang['lang_id'];
                        break;
                    }
                }
                $this->data['missing_translations'] = TranslationsUtils::getMisingTranslationsByAppIdAndLang($URL_params[0], $lang_id);
                $this->data['translations'] = TranslationsUtils::getAllTranslationsByAppIdAndLang($URL_params[0], $lang_id);
            } else {
                $this->data['missing_translations'] = TranslationsUtils::getMisingTranslationsByAppId($URL_params[0]);
                $this->data['translations'] = TranslationsUtils::getAllTranslationsByAppId($URL_params[0]);
            }
        } else {
            $this->redirectToError("You can not edit translations of this app!", 403);
        }
    }

}
