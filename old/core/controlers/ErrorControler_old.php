<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ErrorControler
 *
 * @author jakub
 */
class ErrorControler extends Controler {

    //put your code here
    public function execute($parsed_URL) {
        $this->userControler = new User();
        $this->data['user'] = $this->userControler->getLoggedUser();
        $this->data['error_messages'] = $this->returnErrorsMessages();

        $this->view = "error";
        switch ($parsed_URL[0]) {
            case 401:
                header("HTTP/1.0 401 Unauthorized");
                // Hlavička stránky
                $this->hlavicka['titulek'] = "Unauthorized 401 - " . APPNAME;
                $this->data['error_text'] = "You can not access this page! <br /><br />" . json_encode($this->data['error_messages']);
                break;
            case 403:
                header("HTTP/1.0 403 Forbidden");
                // Hlavička stránky
                $this->hlavicka['titulek'] = "Forbidden action 403 - ";
                $this->data['error_text'] = json_encode($this->data['error_messages']);
                break;
            default:
            case 404:
                header("HTTP/1.0 404 Not Found");
                // Hlavička stránky
                $this->hlavicka['titulek'] = 'Error 404';
                $this->data['error_text'] = "This page is missing... If it should be there, contact admin... Or try it again.";
                break;
            case 423:
                header("HTTP/1.0 423 Locked");
                // Hlavička stránky
                $this->hlavicka['titulek'] = 'Error 423 - BANNED IP';
                $this->data['banned_ip'] = SpravceIP::getBannedIP();
                $this->data['error_text'] = "Your IP has been banned! Ban will expire " . $this->data['banned_ip']['ban_expire']
                        . "<br / > <strong>Ban Note: </strong>" . $this->data['banned_ip']['visible_note'];
                break;
        }
        $this->data['error_name'] = $this->hlavicka['titulek'];
    }

}
