<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class LoginControler extends Controler {

    public $defaultTemplate = false;
    public $Template = "loginTemplate";

    //0-> remote 1 -> app 2 -> ask_token 3 -> param
    public function execute($URL_params) {
        $this->userControler = new User();
        if ($URL_params[0] == "adminlogin" && $URL_params[1] == "logback") {
            $this->userControler->disableAdminLogin();
            $this->addMessage("admin login has been disabled", "link");
            $this->redirect("admin"); //adminlogin/logback
        } else if ($URL_params[0] == "remote") {
            array_shift($URL_params);
            $this->remoteLogin($URL_params);
        } else if ($this->userControler->isUserLoggedIn()) {
            $this->redirect("dashBoard");
        } else if ($URL_params['code']) {
            $this->loginCode();
        } else if ($URL_params['code']) {
            $this->login_lost_password();
        } else {
            $this->loginViaPassword();
        }
    }

    public function remoteLogin($URL_params) {
        //header("Location: https://" . LDAPSERVER . "/login/remote/" . htmlspecialchars(APPNAME) . "//?redirect=" . $redirect);
        $this->view = "loginApp";
        $this->data['requested_app'] = urldecode($URL_params[0]);
        if (!$this->userControler->isUserLoggedIn()) {
            $this->view = "loginForm";
            if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['recaptcha'])) {
                $this->loginViaPassword(urldecode($URL_params[0]));
            }
        } elseif ($this->userControler->canUserAccessThisApp(AppsUtils::getAppRuleByName(urldecode($URL_params[0])))) {
            $this->view = "loginApp";
            $this->data['user'] = $this->userControler->getLoggedUser();
            if ($_POST['loginToApp'] == 1) {
                $token = TokenUtils::generateTokenForRemoteLogin(urldecode($URL_params[0]),
                                $this->userControler->getloggedUserId());
                $app_adress = AppsUtils::getAppAdressByName(urldecode($URL_params[0]));
                $redirect = "";
                if (strpos($_SERVER['REQUEST_URI'], "redirect=") > 1) {
                    $redirect = "/?redirect=" . substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], "redirect=") + 9);
                }
                header("Location: https://" . $app_adress . "/login/ldap/" . $token . $redirect);
                header("Connection: close");
                exit;
            }
        } else {
            $this->view = "loginApp";
            $this->data['user'] = $this->userControler->getLoggedUser();
            $this->addMessage("You can not access this app", "danger");
        }
    }

    public function loginViaPassword($redirect_to = "TopEscape login system") {
        if (SecurityUtils::canLogin()) {
            $this->view = "loginForm";
        } else {
            $this->addMessage("To many wrong login. Try it later!", "danger");
            return;
        }
        $this->data['requested_app'] = $redirect_to;
        if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['recaptcha'])) {
            try {
                $user = $this->userControler->getUserByNameAndPassword($_POST['username'], $_POST['password']);
                $recaptcha = TokenUtils::authorizeAndGetRecaptcha($_POST['recaptcha']);
                if ($recaptcha['score'] < 0.7 && $user['two_factor'] != 0) {
                    $_SESSION['temp']['login']['username'] = $user['login_name'];
                    $_SESSION['temp']['login']['token'] = StringUtils::generateLoginTokenUtils();
                    MailTool::sendLoginCodeMail($user['mail']);
                    $this->redirect("login/code");
                } else {
                    $this->userControler->loginViaUsername($user['login_name']);
                    SecurityUtils::resetWrongLogin();
                    if ($redirect_to != "TopEscape login system") {
                        if (strpos($_SERVER['REQUEST_URI'], "redirect=") > 1) {
                            $redirect = "/?redirect=" . substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], "redirect=") + 9);
                        }
                        $this->redirect("login/remote/" . $redirect_to . $redirect);
                    }
                    $this->redirect("dashBoard");
                }
            } catch (loginException $exc) {
                $this->addMessage($exc->getMessage(), "danger");
                SecurityUtils::addWrongLogin(true, $exc->getCode());
            }
        }
    }

}
