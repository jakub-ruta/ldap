<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jakub
 */
abstract class Controler {

    // Pole, jehož indexy jsou poté viditelné v šabloně jako běžné proměnné
    protected $data = array();
    // Název šablony bez přípony
    protected $view = "";
    // Hlavička HTML stránky
    protected $hlavicka = array('title' => 'LDAP TopEscape.cz');
    protected $userControler;
    public $defaultTemplate = true;
    public $session_logger_class;

    public function setSession_logger_class($session_logger_class) {
        $this->session_logger_class = $session_logger_class;
    }

    // Ošetří proměnnou pro výpis do HTML stránky
    private function sanitize($x = null) {
        if (!isset($x)) {
            return null;
        } elseif (is_string($x)) {
            return htmlspecialchars($x, ENT_QUOTES);
        } elseif (is_array($x)) {
            foreach ($x as $k => $v) {
                $x[$k] = $this->sanitize($v);
            }
            return $x;
        } else {
            return $x;
        }
    }

    // Vyrenderuje pohled
    public function renderView() {
        if ($this->pohled) {
            $this->data['DB_query_count'] = Db::getQueryCount();
            $this->data['DB_query_debug'] = Db::getQueryDebug();
            $this->data['messages'] = $this->returnMessages();
            extract($this->sanitize($this->data));
            extract($this->data, EXTR_PREFIX_ALL, "");
            require("core/views/" . $this->pohled . ".phtml");
        }
    }

    // Přidá zprávu pro uživatele
    public function addMessage($message, $color = "warning") {
        if (isset($_SESSION['temp']['messages'])) {
            $_SESSION['temp']['messaes'][] = array("message" => $message, "color" => $color);
        } else {
            $_SESSION['temp']['messages'][] = array("message" => $message, "color" => $color);
        }
    }

    // Vrátí zprávy pro uživatele
    public function returnMessages() {
        if (isset($_SESSION['temp']['messages'])) {
            $messages = $_SESSION['temp']['messages'];
            unset($_SESSION['temp']['messages']);
            return $messages;
        } else {
            return array();
        }
    }

    // Přesměruje na dané URL
    public function redirect($url) {
        header("Location: /$url");
        header("Connection: close");
        exit;
    }

    public function redirectToError($error_message, $code) {
        $_SESSION['temp']['error_messages'][] = $error_message;
        header("Location: /error/$code");
        header("Connection: close");
        exit;
    }

    public function returnErrorsMessages() {
        if (isset($_SESSION['temp']['error_messages'])) {
            $messages = $_SESSION['temp']['error_messages'];
            unset($_SESSION['temp']['error_messages']);
            return $messages;
        } else {
            return array();
        }
    }

    public function getData() {
        return $this->data;
    }

    // Hlavní metoda controlleru
    abstract function execute($parametry);
}
