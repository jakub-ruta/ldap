<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AjaxControler
 *
 * @author jakub
 */
class CronControler extends Controler {

//put your code here
    public function execute($URL_params) {
        switch (array_shift($URL_params)) {
            case 'tokenUpdater' :
                $this->tokenUpdaterFunction($URL_params);
                break;
            case 'translationsUpdater' :
                $this->translationsUpdaterFunction($URL_params);
                break;
            case 'translationsUploader' :
                $this->translationsUploaderFunction($URL_params);
                break;
        }
        die;
    }

    public function tokenUpdaterFunction($URL_params) {
        $old_tokens = TokenUtils::getAllOldTokens();

        foreach ($old_tokens as $old_token) {
            try {
                $setup_token = TokenUtils::getSettingsTokenFor($old_token['adress']);
                $new_token = StringUtils::generate_string(120);
                $new_setup_token = StringUtils::generate_string(120);
                $request = array("action" => "setToken", "type" => $old_token['function'],
                    "old_token" => $old_token['token'], "new_token" => $new_token,
                    "auth_token" => $setup_token, "new_auth_token" => $new_setup_token);
                $url = "https://" . $old_token['adress'] . "/ajax/set/local/token";
                $response = json_decode($this->send($url, $request), true);
                ActionLogUtils::getInstance()->logCron(json_encode(array("old" => $old_token, "responce" => $response)));
                bdump($response);
                if ($response['status'] == "success") {
                    TokenUtils::updateToken($old_token['function'], $old_token['adress'], $old_token['token'],
                            $new_token);
                    TokenUtils::updateToken("setup", $old_token['adress'], $setup_token,
                            $new_setup_token);
                }
            } catch (tokenException $ex) {
                echo('missing roken');
            }
        }
    }

    private function send($url, $post) {
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handler, CURLOPT_POST, 1);
        curl_setopt($handler, CURLOPT_POSTFIELDS, $this->urlEncodePost($post));
        curl_setopt($handler, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        $obsah = curl_exec($handler);
        curl_close($handler);
        return $obsah;
    }

    private function urlEncodePost($str) {
        $fields_string = "";
        foreach ($str as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        return $fields_string;
    }

    public function translationsUpdaterFunction($URL_params) {
        ActionLogUtils::getInstance()->logCron("translations");
        $apps_langs = array();
        $new_strings = TranslationsUtils::getNewStrings();
        foreach ($new_strings as $str) {
            if (array_key_exists($str['app_id'], $apps_langs)) {
                $langs = $apps_langs[$str['app_id']];
            } else {
                $apps_langs[$str['app_id']] = $langs = TranslationsUtils::getAppLangsByAppId($str['app_id']);
            }
            foreach ($langs as $lang) {
                TranslationsUtils::addTranslations($lang['lang_id'], $str['string_id'], $str['original'],
                        ($lang['lang_code'] == "en" ? 0 : 1));
                TranslationsUtils::removeNewFromString($str['string_id']);
            }
        }
    }

    public function translationsUploaderFunction($URL_params) {
        ActionLogUtils::getInstance()->logCron("translations-uploader");
        $apps = AppsUtils::getAppsWithUpdateRequest();
        foreach ($apps as $app) {
            $translations = array();
            $langs = TranslationsUtils::getAppLangsByAppId($app['id']);
            foreach ($langs as $lang) {
                if ($lang['lang_can_be_uploaded']) {
                    $translations[$lang['lang_code']] = TranslationsUtils::getAllTranslationsByLangId($lang['lang_id']);
                }
            }
            if (count($translations) > 0) {

                $response = $this->send("https://" . $app['adress'] . "/ajax/set/local/translations/download",
                        array("token" => TokenUtils::getRemoteToken($app['adress'], "set"), "action" => "translations_update",
                            "data" => json_encode($translations)));
                bdump($response);
                if (json_decode($response, true)['status'] == "success") {

                    AppsUtils::setUpdateReqest($app['id']);
                }
            }
        }
    }

}
