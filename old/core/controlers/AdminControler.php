<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminControler
 *
 * @author jakub
 */
class AdminControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->userControler = new User();

        if (!$this->userControler->isUserLoggedIn()) {
            $this->redirect("login/?redirect=" . $_SERVER['REQUEST_URI']);
        }
        if ($this->userControler->getRuleValue("users", 0) == 0) {
            $this->redirectToError("You do not have right.", 401);
        }
        $this->data['user'] = $this->userControler->getLoggedUser();
        if (isset($URL_params[0])) {
            $this->data['user_editable_fields'] = array("name", "nickname", "mail", "surname", "birth_date",
                "mobile", "admin_level", "can_change_profile", "locale");
            if ($this->userControler->getAdminLevel() > 3) {
                $this->data['user_editable_fields'][] = "login_name";
            }
            if (!empty($_POST)) {
                $this->executeRights($URL_params[0]);
            }
            if ($URL_params[1] == "rebase_rules") {
                $this->userControler->rebaseRules();
                $this->addMessage("Rebase rules has been complete.", "success");
                $this->redirect("admin/" . $URL_params[0]);
            } else if ($URL_params[1] == "admin_login") {
                if ($this->userControler->getRuleValue("admin_login", 0) == 0) {
                    $this->redirectToError("You do not have right to use admin login.", 401);
                }
                if ($this->userControler->getAdminLevel() <
                        $this->userControler->getUserById($URL_params[0])['admin_level'] &&
                        $this->userControler->getAdminLevel() < 4) {
                    $this->redirectToError("You do not have right to log in as this user!", 401);
                }
                $this->addMessage("Admin login has been activated", "primary");
                $this->userControler->ActivateAdminLogin($URL_params[0]);
                $this->redirect("dashBoard");
            }
            $this->view = "user";

            $this->data['user_edit'] = $this->userControler->getOneUsers($URL_params[0]);
            $this->data['rights'] = $this->userControler->getUserRights($URL_params[0]);
        } else {
            $this->view = "users";
            $this->data['users'] = $this->userControler->getAllUsers();
        }
    }

    public function executeRights($user_id) {
        if ($this->userControler->getRuleValue("rights", 0) == 0) {
            $this->redirectToError("You do not have right to update users!", 401);
        }
        if ($this->userControler->getAdminLevel() <
                $this->userControler->getUserById($user_id)['admin_level'] &&
                $this->userControler->getAdminLevel() < 4) {
            $this->redirectToError("You do not have right edit this user!", 401);
        }
        if ($_POST['admin_level'] >= $this->userControler->getAdminLevel() && $this->userControler->getAdminLevel() < 4) {
            if ($user_id != $this->userControler->getloggedUserId()) {
                $_POST['admin_level'] = $this->userControler->getAdminLevel() - 1;
                $this->addMessage("You can not set admin_level to this value! Value has been corrected", "warning");
            } else {
                $_POST['admin_level'] = $this->userControler->getAdminLevel();
            }
        }
        $rule_list = $this->userControler->getRulesList();

        $this->userControler->changeUserInformations(array_intersect_key($_POST,
                        array_flip($this->data['user_editable_fields'])), $user_id);
        foreach ($rule_list as $rule_l) {
            if ($rule_l['granteable'] == 1 || $this->userControler->getAdminLevel() > 3) {
                $val = 0;
                if ($_POST['admin::' . $rule_l['name']]) {
                    $val = 1;
                }
                $data = Array("user_id" => $user_id,
                    "right_id" => $rule_l['id'],
                    "value" => $val,
                );
                $this->userControler->updateUserRights($data);
            }
        }


        $this->addMessage("User information has been changed.", "success");
        $this->redirect("admin/" . $user_id);
    }

}
