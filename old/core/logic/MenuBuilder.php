<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuBuilder
 *
 * @author jakub
 */
class MenuBuilder {

    public static function build_menu() {

        $mb = MenuBuilder_new::getInstance();
        $mb->addToMenu("Eshop", "Eshop", "shop", false, "fas fa-shopping-basket");
        $mb->addToMainMenu("Eshop", "Delivery options", "page/del", false, "fas fa-paragraph");
        $mb->addToMainMenu("Eshop", "Terms and Conditions", "page/agb", false, "fas fa-paragraph");
        $mb->addToMainMenu("Eshop", "Terms of complaint", "page/reklam", false, "fas fa-paragraph");
        $mb->addToMainMenu("Eshop", "Privacy Policy", "page/gdpr", false, "fas fa-paragraph");
        if (User::get_instance()->getRuleValue("inventory", "view")) {
            $mb->addToMenu("Inventory", "Inventory DashBoard", "inventory", false, "fas fa-warehouse");
            $mb->addToMenu("Inventory", "Items", "inventory/items", false, "fas fa-boxes");
        }
        if (User::get_instance()->getRuleValue("customers_orders", "view")) {
            $mb->addToMenu("Customers", "Orders", "customers/orders", false, "fas fa-shopping-basket");
        }
        if (User::get_instance()->getRuleValue("customers_users", "view")) {
            $mb->addToMenu("Customers", "Customers accounts", "customers/users", false, "fas fa-user");
        }
        if (User::get_instance()->getRuleValue("payment", "view")) {
            $mb->addToMenu("Payment", "Payment DashBoard", "payment", false, "fas fa-money-check-alt");
        }

        if (User::get_instance()->getRuleValue("crm", "view")) {
            $mb->addToMenu("CRM", "CRM DashBoard", "crm", false, "fas fa-coins");
        }
        if (User::get_instance()->getRuleValue("crm_users", "view")) {
            $mb->addToMenu("CRM", "Users CRM campaign", "crm/users", false, "fas fa-ad");
        }
        if (User::get_instance()->getRuleValue("crm_items", "view")) {
            $mb->addToMenu("CRM", "Items CRM campaign", "crm/items", false, "fas fa-ad");
        }
        if (User::get_instance()->getRuleValue("crm_promo", "view")) {
            $mb->addToMenu("CRM", "Promo code", "promo_codes", false, "fas fa-code-branch");
        }
        if (User::get_instance()->getRuleValue("customers_baskets", "view")) {
            $mb->addToMainMenu("Debug", "Customers baskets", "customers/baskets", false, "fab fa-dev");
        }
    }

    //put your code here
}
