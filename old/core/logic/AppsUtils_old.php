<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppsUtils
 *
 * @author jakub
 */
class AppsUtils_old {

    public static function getAllApps($user_id) {
        $apps = Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE 1 ORDER BY order_key ASC');
        foreach ($apps as $key => $app) {
            $apps[$key]['responsible_staff'] = self::getResponsibleStaffByAppId($app['id']);
            if (!$app['external'] && $app['has_translations']) {
                if (self::can_user_edit_translations($user_id, $app['id'])) {
                    $apps[$key]['can_user_edit_translations'] = 1;
                } else {
                    $apps[$key]['can_user_edit_translations'] = 0;
                }
            }
        }
        return $apps;
    }

    public static function getResponsibleStaffByAppId($app) {
        return Db::queryAll('SELECT ' . TABLEPREFIX . 'applications_responsible_staff.position, '
                        . '' . TABLEPREFIX . 'applications_responsible_staff.visible, '
                        . '' . TABLEPREFIX . 'applications_responsible_staff.translations, '
                        . '' . TABLEPREFIX . 'users_personal_informations.login_name, '
                        . '' . TABLEPREFIX . 'users_personal_informations.nickname, '
                        . '' . TABLEPREFIX . 'users_personal_informations.mail '
                        . 'FROM ' . TABLEPREFIX . 'applications_responsible_staff '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'users_personal_informations ON '
                        . '' . TABLEPREFIX . 'applications_responsible_staff.user_id=' . TABLEPREFIX . 'users_personal_informations.user_id '
                        . 'WHERE ' . TABLEPREFIX . 'applications_responsible_staff.app_id=?',
                        $app);
    }

    public static function getAppByRule($rule) {
        return Db::queryOne('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE rule_name=?', $rule);
    }

    public static function getAppById($id) {
        return Db::queryOne('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE id=?', $id);
    }

    public static function getAppByCode($name) {
        return Db::queryOne('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE name=?', $name);
    }

    public static function getAppRuleByName($appName) {
        return Db::querySingle('SELECT rule_name FROM ' . TABLEPREFIX . 'applications WHERE name=? ',
                        htmlspecialchars_decode($appName));
    }

    public static function getAppAdressByName($URL_params) {
        return Db::querySingle('SELECT adress FROM ' . TABLEPREFIX . 'applications WHERE name=? ',
                        htmlspecialchars_decode($URL_params));
    }

    public static function getAllAppsForTranslations($id) {
        $apps = Db::queryAll('SELECT *,' . TABLEPREFIX . 'applications.id AS app_id  FROM ' . TABLEPREFIX . 'applications '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'applications_responsible_staff ON ('
                        . TABLEPREFIX . 'applications_responsible_staff.app_id=' . TABLEPREFIX . 'applications.id AND '
                        . TABLEPREFIX . 'applications_responsible_staff.user_id=?) '
                        . ' WHERE external=0 ORDER BY order_key ASC', $id);

        return $apps;
    }

    public static function can_user_edit_translations($user_id, $app_id) {
        $can = Db::querySingle('SELECT translations FROM ' . TABLEPREFIX . 'applications_responsible_staff'
                        . ' WHERE user_id=? AND app_id=? ',
                        $user_id, $app_id);
        if (isset($can)) {
            return $can;
        } else {
            return false;
        }
    }

    public static function request_update($URL_params) {
        if (!self::getAppById($URL_params)['has_translations'])
            throw new Exception("This app do not have translations");

        return Db::query('UPDATE ' . TABLEPREFIX . 'applications SET'
                        . ' translation_update_request=?, translations_update_request_time=?'
                        . ' WHERE id=?', 1, time(), $URL_params);
    }

    public static function abort_update($URL_params) {
        if (!self::getAppById($URL_params)['has_translations'])
            throw new Exception("This app do not have translations");
        return Db::query('UPDATE ' . TABLEPREFIX . 'applications SET'
                        . ' translation_update_request=?, translations_update_request_time=?'
                        . ' WHERE id=?', 0, time(), $URL_params);
    }

    public static function getAppsWithUpdateRequest() {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'applications WHERE has_translations=? '
                        . 'AND translation_update_request=? AND translations_update_request_time<?',
                        1, 1, time() + 60 * 60);
    }

    public static function setUpdateReqest($app_id) {
        return Db::query('UPDATE ' . TABLEPREFIX . 'applications SET'
                        . ' translation_update_request=?, trasnlations_last_upload=? '
                        . ' WHERE id=?', 0, date("Y-m-d H:i:s"), $app_id);
    }

    public static function getResponsibleStaffByAppIdAll($app) {
        $db = MysqliDb::getInstance();
        $db->join("users_personal_informations r", "l.user_id=r.user_id");
        $db->where("app_id", $app);
        return $db->get("applications_responsible_staff l", null, "l.*, r.login_name, r.mail, r.nickname,"
                        . " r.admin_level, r.disabled_account, r.avatar");
    }

    public static function update_staff($id, $col, $data) {
        $array = array("position", "visible", "translations", "in_app_admin_level");
        if (!in_array($col, $array)) {
            throw new Exception("This operation is not allowed!");
        }
        $db = MysqliDb::getInstance();
        $db->where("id", $id);
        $db->update("applications_responsible_staff", array($col => $data), 1);
        echo(json_encode($db->getLastQuery()));
        die;
    }

    public static function getAdminLevelByResponsibleStaffByAppNameUserId($user_id, $app_name) {
        $db = MysqliDb::getInstance();
        $db->where("name", $app_name);
        $app_id = $db->getValue("applications", "id");
        $db->where("app_id", $app_id);
        $db->where("user_id", $user_id);
        return $db->getValue("applications_responsible_staff", "in_app_admin_level");
    }

    public static function add_staff($app_id, $user_id) {
        $db = MysqliDb::getInstance();
        $db->insert("applications_responsible_staff", array("app_id" => $app_id, "user_id" => $user_id));
        echo(json_encode($db->getLastQuery()));
        die;
        return true;
    }

}
