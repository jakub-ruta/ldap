<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SessionLoggerCollectorClass
 *
 * @author jakub
 */
class SessionLoggerCollectorClass {

    protected static $id;
    protected static $data = array();
    protected static $start;
    protected static $start_m;

    public function __construct() {
        self::$start = time();
        self::$start_m = microtime(true);
        $data = array("aditional_data" => null, "session_type" => "unknown",
            "event_name" => "Page", "event_type" => "log", "event_action" => "see",
        );

        self::$id = ActionLogUtils::sendToDb($data);
    }

    public function addData($data) {
        if (is_array($data)) {
            self::$data = array_merge(self::$data, $data);
        } else {
            self::$data[] = $data;
        }
    }

    public function __destruct() {
        $qeries = "";
        $post = "";
        if (ENABLELOGQUERIES) {
            $qeries = Db::getQueryDebug();
            $post = json_encode($_POST);
        }
        $this->addData(array("Db" => array("count" => Db::getQueryCount(), "querry" => $qeries), "post" => $post,
            "run_time" => array("time" => time() - self::$start, "micro_time" => microtime(true) - self::$start_m)));
        ActionLogUtils::updateLogWithRaw(self::$data, self::$id);
    }

}
