<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActionLogUtils
 *
 * @author jakub
 */
class ActionLogUtils {

    public static function logWebHooks($raw_data) {
        $data = array("aditional_data" => $raw_data, "session_type" => "webhooks",
            "event_name" => "PUSH", "event_type" => "log", "event_action" => "setStatus",
        );
        self::sendToDb($data);
    }

    public static function logCron($raw_data) {
        $data = array("aditional_data" => $raw_data, "session_type" => "cron",
            "event_name" => "Cron", "event_type" => "log", "event_action" => "token",
        );
        self::sendToDb($data);
    }

    public static function logTest($raw_data) {
        $data = array("aditional_data" => $raw_data, "session_type" => "admin",
            "event_name" => "Test", "event_type" => "log", "event_action" => "",
        );
        self::sendToDb($data);
    }

    public static function sendToDb($data) {
        $static = array("page" => $_SERVER['REQUEST_URI'],
            "method" => $_SERVER['REQUEST_METHOD'],
            "IP" => SpravceIP::getUserIpAddr(),
            "Browser" => SpravceIP::getUserBrowser());
        $d = array_merge($static, $data);
        Db::insert(TABLEPREFIX . 'action_log', $d);
        return Db::getLastId();
    }

    public static function updateLogWithRaw($data, $id) {
        Db::query('UPDATE ' . TABLEPREFIX . 'action_log SET aditional_data=? WHERE id=?',
                json_encode($data), $id);
    }

}
