<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TranslatioonsUtils
 *
 * @author jakub
 */
class TranslationsUtils {

    public static function getSupportedLangByAppId($app_id) {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'applications_lang WHERE app_id=? ',
                        $app_id);
    }

    public static function getMisingTranslationsByAppId($app_id) {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'lang_translations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'applications_lang '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.lang_id=' . TABLEPREFIX . 'applications_lang.lang_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.string_id=' . TABLEPREFIX . 'lang_strings.string_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings_group '
                        . 'ON ' . TABLEPREFIX . 'lang_strings.group_id=' . TABLEPREFIX . 'lang_strings_group.group_id '
                        . 'WHERE ' . TABLEPREFIX . 'lang_translations.tran_new=? AND ' . TABLEPREFIX . 'lang_strings.app_id=?', 1, $app_id);
    }

    public static function update_translation($URL_params, $new) {
        return Db::query('UPDATE ' . TABLEPREFIX . 'lang_translations SET'
                        . ' translations=?, tran_new=?'
                        . ' WHERE tran_id=?', $new, 0, $URL_params);
    }

    public static function getTranslation($params) {
        return Db::queryOne('SELECT * FROM ' . TABLEPREFIX . 'lang_translations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'applications_lang '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.lang_id=' . TABLEPREFIX . 'applications_lang.lang_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.string_id=' . TABLEPREFIX . 'lang_strings.string_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings_group '
                        . 'ON ' . TABLEPREFIX . 'lang_strings.group_id=' . TABLEPREFIX . 'lang_strings_group.group_id '
                        . 'WHERE ' . TABLEPREFIX . 'lang_translations.tran_id=?', $params);
    }

    public static function getAllTranslationsByAppId($app_id) {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'lang_translations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'applications_lang '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.lang_id=' . TABLEPREFIX . 'applications_lang.lang_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.string_id=' . TABLEPREFIX . 'lang_strings.string_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings_group '
                        . 'ON ' . TABLEPREFIX . 'lang_strings.group_id=' . TABLEPREFIX . 'lang_strings_group.group_id '
                        . 'WHERE ' . TABLEPREFIX . 'lang_strings.app_id=?', $app_id);
    }

    public static function addOriginals($app_id, $strings) {

        foreach ($strings as $value) {
            if (!self::originalExists($app_id, $value)) {
                Db::insert(TABLEPREFIX . 'lang_strings', array("app_id" => $app_id, "original" => $value));
            }
        }
    }

    public static function originalExists($app_id, $value) {
        $token_1 = Db::querySingle('SELECT string_id FROM ' . TABLEPREFIX . 'lang_strings '
                        . 'WHERE app_id=? AND original=? ', $app_id, $value);
        if ($token_1 != "" && $token_1 != null) {
            return true;
        }
        return false;
    }

    public static function getNewStrings() {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'lang_strings WHERE new=? ',
                        1);
    }

    public static function getAppLangsByAppId($app_id) {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'applications_lang WHERE app_id=? ',
                        $app_id);
    }

    public static function addTranslations($lang_id, $string_id, $original, $new = 1) {
        $data = array("lang_id" => $lang_id, "string_id" => $string_id, "translations" => $original,
            "tran_new" => $new);
        return Db::insert(TABLEPREFIX . 'lang_translations', $data);
    }

    public static function removeNewFromString($string_id) {
        return Db::query('UPDATE ' . TABLEPREFIX . 'lang_strings SET'
                        . ' new=?'
                        . ' WHERE string_id=?', 0, $string_id);
    }

    public static function getAllTranslationsByLangId($lang_id) {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'lang_translations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.string_id=' . TABLEPREFIX . 'lang_strings.string_id '
                        . 'WHERE ' . TABLEPREFIX . 'lang_translations.lang_id=?', $lang_id);
    }

    public static function deleteTranslations($params) {
        return array();
    }

    public static function getMisingTranslationsByAppIdAndLang($app_id, $lang) {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'lang_translations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'applications_lang '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.lang_id=' . TABLEPREFIX . 'applications_lang.lang_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.string_id=' . TABLEPREFIX . 'lang_strings.string_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings_group '
                        . 'ON ' . TABLEPREFIX . 'lang_strings.group_id=' . TABLEPREFIX . 'lang_strings_group.group_id '
                        . 'WHERE ' . TABLEPREFIX . 'lang_translations.tran_new=? AND'
                        . ' ' . TABLEPREFIX . 'lang_strings.app_id=? AND '
                        . ' ' . TABLEPREFIX . 'lang_translations.lang_id=?', 1, $app_id, $lang);
    }

    public static function getAllTranslationsByAppIdAndLang($app_id, $lang_id) {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'lang_translations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'applications_lang '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.lang_id=' . TABLEPREFIX . 'applications_lang.lang_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings '
                        . 'ON ' . TABLEPREFIX . 'lang_translations.string_id=' . TABLEPREFIX . 'lang_strings.string_id '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'lang_strings_group '
                        . 'ON ' . TABLEPREFIX . 'lang_strings.group_id=' . TABLEPREFIX . 'lang_strings_group.group_id '
                        . 'WHERE ' . TABLEPREFIX . 'lang_strings.app_id=? AND '
                        . ' ' . TABLEPREFIX . 'lang_translations.lang_id=?', $app_id, $lang_id);
    }

    public static function getAllLangs() {
        $db = MysqliDb::getInstance();
        return $db->get("applications_lang");
    }

    public static function getLang($id) {
        $db = MysqliDb::getInstance();
        $db->where("lang_id", $id);
        return $db->getOne("applications_lang");
    }

    public static function addNewLang($data) {
        $db = MysqliDb::getInstance();
        $db->insert("applications_lang", $data);
        $last_id = $db->getInsertId();
        $strings = self::getAllStringsByAppId($data['app_id']);
        foreach ($strings as $value) {
            self::addTranslations($last_id, $value['string_id'], $value['original']);
        }
    }

    public static function getAllStringsByAppId($data) {
        $db = MysqliDb::getInstance();
        $db->where("app_id", $data);
        return $db->get("lang_strings");
    }

    public static function updateLang($lang_id, $data) {
        $db = MysqliDb::getInstance();
        $db->where("lang_id", $lang_id);
        $db->update("applications_lang", $data);
    }

}
