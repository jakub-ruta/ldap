<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author jakub
 */
class User_old {

    /**
     * Static instance of self
     *
     * @var User
     */
    protected static $_instance;
    protected static $_sess_id;

    static function get_instance() {
        return self::$_instance;
    }

    /**
     * A method of returning the static instance to allow access to the
     * instantiated object from within another class.
     * Inheriting this class would require reloading connection info.
     *
     * @uses $users = User::getInstance();
     *
     * @return User Returns the current instance.
     */
    public static function getInstance() {
        return self::$_instance;
    }

    function __construct() {
        self::$_instance = $this;
        if ($_SESSION['system']['user']['logged_in']) {
            $_SESSION['user'] = $this->getUserById($this->getloggedUserId());
            $this->loadRules();
        }
    }

    public function getLoggedUser() {
        return $_SESSION['user'];
    }

    public function getloggedUserId() {
        return $_SESSION['user']['user_id'];
    }

    public static function getUserId() {
        return $_SESSION['user']['user_id'];
    }

    public function isUserLoggedIn() {
        if ($_SESSION['system']['user']['logged_in'] == 1) {
            return true;
        }
        return false;
    }

    public function Logout() {
        $_SESSION['system']['user']['logged_in'] = 0;
        $_SESSION['adminlogin']['isActive'] = 0;
        unset($_SESSION['adminlogin']['data']);
        unset($_SESSION['user']);
    }

    public function getUserByNameAndPassword($username, $password) {
        $user = Db::queryOne('SELECT ' . TABLEPREFIX . 'users_personal_informations.*, '
                        . TABLEPREFIX . 'users_creditals.password_type, '
                        . TABLEPREFIX . 'users_creditals.salt1, '
                        . TABLEPREFIX . 'users_creditals.salt2 '
                        . 'FROM ' . TABLEPREFIX . 'users_personal_informations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'users_creditals '
                        . 'ON ' . TABLEPREFIX . 'users_creditals.user_id=' . TABLEPREFIX . 'users_personal_informations.user_id '
                        . 'WHERE ' . TABLEPREFIX . 'users_personal_informations.login_name=? ',
                        $username);
        if (!isset($user['user_id'])) {
            throw new loginException("Wrong password or username");
        }
        switch ($user['password_type']) {
            case 'old_hash':
                $pass = $password . $user['salt1'];
                break;
            default:
                throw new loginException("Wrong password or username");
        }
        $verived_user = Db::queryOne('SELECT ' . TABLEPREFIX . 'users_personal_informations.*, '
                        . TABLEPREFIX . 'users_creditals.password_type '
                        . 'FROM ' . TABLEPREFIX . 'users_personal_informations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'users_creditals '
                        . 'ON ' . TABLEPREFIX . 'users_creditals.user_id=' . TABLEPREFIX . 'users_personal_informations.user_id '
                        . 'WHERE ' . TABLEPREFIX . 'users_personal_informations.login_name=? AND '
                        . '' . TABLEPREFIX . 'users_creditals.password=SHA(?)',
                        $username, $pass);
        if (!isset($verived_user['user_id'])) {
            throw new loginException("Wrong password or username", $user['id']);
        }
        return $verived_user;
    }

    public function loginViaUsername($username) {
        $_SESSION['user'] = Db::queryOne('SELECT ' . TABLEPREFIX . 'users_personal_informations.*, '
                        . TABLEPREFIX . 'users_creditals.password_type '
                        . 'FROM ' . TABLEPREFIX . 'users_personal_informations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'users_creditals '
                        . 'ON ' . TABLEPREFIX . 'users_creditals.user_id=' . TABLEPREFIX . 'users_personal_informations.user_id '
                        . 'WHERE ' . TABLEPREFIX . 'users_personal_informations.login_name=? '
                        , $username);
        $this->loadRules();
        $_SESSION['system']['user']['logged_in'] = 1;
        $_SESSION['adminlogin']['isActive'] = 0;
    }

    public function getUserById($user_id) {
        $user = Db::queryOne('SELECT user_id, login_name, nickname, mail, locale, admin_level, avatar '
                        . 'FROM ' . TABLEPREFIX . 'users_personal_informations '
                        . 'WHERE user_id=? '
                        , $user_id);
        if (!isset($user['user_id'])) {
            throw new LogicException("cannot get user");
        }
        return $user;
    }

    public function getRulesList() {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'user_rules_list WHERE 1 ORDER BY remote ASC');
    }

    public function getUserRightsFromDb($id) {
        $rules = Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'user_rules WHERE user_id=? ORDER BY right_id DESC', $id);
        foreach ($rules as $rule) {
            $rights[$rule['right_id']] = $rule;
        }
        return $rights;
    }

    public function loadRules() {
        $rules_list = $this->getRulesList();
        $user_right = $this->getUserRightsFromDb($this->getloggedUserId());
        foreach ($rules_list as $rule) {
            if ($rule['remote'] == 1) {
                $_SESSION['user']['rights'][$rule['name']] = $this->ruleValue($user_right, $rule['id']);
            } else {
                $_SESSION['user']['rights']['admin'][$rule['name']]['edit'] = $this->ruleValue($user_right, $rule['id']);
            }
        }
    }

    private function ruleValue($user_right, $rule_id) {
        if (isset($user_right[$rule_id])) {
            if (new DateTime($user_right[$rule_id]['valid_from']) < new DateTime() &&
                    new DateTime($user_right[$rule_id]['valid_to']) > new DateTime()) {
                return $user_right[$rule_id]['value'];
            }
        } else {
            return 0;
        }
    }

    public function getRuleValue($rule_name, $remote = 1) {
        if ($this->getAdminLevel() > 3)
            return 1;
        if ($remote == 1) {
            if (isset($_SESSION['user']['rights'][$rule_name]))
                return $_SESSION['user']['rights'][$rule_name];
            return 0;
        } else {
            if (isset($_SESSION['user']['rights']['admin'][$rule_name]))
                return $_SESSION['user']['rights']['admin'][$rule_name]['edit'];
            return 0;
        }
    }

    public function getAdminLevel() {

        return $_SESSION['user']['admin_level'];
    }

    public function setNewPassword($pass) {
        $salt1 = StringUtils::generate_string(15);
        $salt2 = StringUtils::generate_string(20);
        $password = $pass . $salt1;
        Db::query('UPDATE ' . TABLEPREFIX . 'users_creditals SET password=SHA1(?), salt1=?, salt2=?,'
                . ' password_type=? '
                . 'WHERE user_id=?', $password, $salt1, $salt2, "old_hash", $this->getloggedUserId());
    }

    public function canUserAccessThisApp($rule) {
        if ($this->getRuleValue($rule) != 0) {
            return true;
        }
        return false;
    }

    public function getAllUsers() {
        return Db::queryAll('SELECT ' . TABLEPREFIX . 'users_personal_informations.*, '
                        . TABLEPREFIX . 'users_creditals.password_type '
                        . 'FROM ' . TABLEPREFIX . 'users_personal_informations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'users_creditals '
                        . 'ON ' . TABLEPREFIX . 'users_creditals.user_id=' . TABLEPREFIX . 'users_personal_informations.user_id '
        );
    }

    public function getOneUsers($id) {
        $db = MysqliDb::getInstance();
        $db->where("user_id", $id);
        return $db->getOne("users_personal_informations");
    }

    public function getUserRights($id) {
        $db = MysqliDb::getInstance();
        $db->join("user_rules r", "l.id=r.right_id", "LEFT");
        $db->where("r.user_id", $id);
        $db->orWhere("r.value", null, " IS");
        $db->orderBy("l.remote", "ASC");
        $data = $db->get("user_rules_list l");

        return $data;
    }

    public function updateUserRights($data) {
        $db = MysqliDb::getInstance();
        $data["updated"] = $db->now();
        $updateColumns = Array("value", "updated");
        $lastInsertId = "id";
        $db->onDuplicate($updateColumns, $lastInsertId);
        $id = $db->insert('user_rules', $data);
    }

    public function rebaseRules() {
        $db = MysqliDb::getInstance();
        $rules_list = $this->getRulesList();
        $users = $this->getAllUsers();
        foreach ($users as $user) {
            foreach ($rules_list as $rule) {
                $data = Array("user_id" => $user['user_id'],
                    "right_id" => $rule['id'],
                    "value" => 0,
                );
                $id = $db->insert('user_rules', $data);
            }
        }
    }

    public function ActivateAdminLogin($id) {
        $user = $this->getUserById($id);
        $_SESSION['adminlogin']['data'] = $_SESSION['user'];
        $_SESSION['adminlogin']['isActive'] = 1;
        $_SESSION['user'] = Db::queryOne('SELECT ' . TABLEPREFIX . 'users_personal_informations.*, '
                        . TABLEPREFIX . 'users_creditals.password_type '
                        . 'FROM ' . TABLEPREFIX . 'users_personal_informations '
                        . 'LEFT JOIN ' . TABLEPREFIX . 'users_creditals '
                        . 'ON ' . TABLEPREFIX . 'users_creditals.user_id=' . TABLEPREFIX . 'users_personal_informations.user_id '
                        . 'WHERE ' . TABLEPREFIX . 'users_personal_informations.login_name=? '
                        , $user['login_name']);
        $this->loadRules();
        $_SESSION['system']['user']['logged_in'] = 1;
    }

    public function disableAdminLogin() {
        $_SESSION['adminlogin']['isActive'] = 0;
        $_SESSION['user'] = $_SESSION['adminlogin']['data'];
    }

    public function getUserList() {
        $db = MysqliDb::getInstance();
        return $db->get("users_personal_informations", null, "user_id, login_name, nickname");
    }

    public function changeUserInformations($data, $user_id) {
        foreach ($data as $key => $value) {
            if ($value == '') {
                $data[$key] = null;
            }
        }
        $db = MysqliDb::getInstance();
        $db->where("user_id", $user_id);
        $db->update("users_personal_informations", $data);
    }

    public function updateProfile($data, $user_id) {
        foreach ($data as $key => $value) {
            if ($value == '') {
                $data[$key] = null;
            }
        }
        $db = MysqliDb::getInstance();
        $db->where("user_id", $user_id);
        $db->update("users_personal_informations", $data);
        var_dump($db->getLastQuery());
        var_dump($db->getLastError());
    }

}
