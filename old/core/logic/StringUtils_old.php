<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StringUtils
 *
 * @author jakub
 */
class StringUtils {

    public static function generate_string($lenght) {
        $chars = 'abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $count = strlen($chars) - 1;
        $return = "";
        for ($i = 0; $i < $lenght; $i++) {
            $return = $return . $chars[mt_rand(0, $count)];
        }
        return $return;
    }

    public static function create_table($data) {

        $i = 0;
        foreach ($data[0] as $key => $u) {
            $keys [$i] = $key;
            $i++;
        }

        foreach ($data as $u) {

            for ($j = 0; $j < $i; $j++) {
                $table[] = $u[$keys[$j]];
            }
        }

        return array("header" => $keys, "body" => $table, "data" => $data);
    }

}
