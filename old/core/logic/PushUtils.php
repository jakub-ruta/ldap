<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PushUtils
 *
 * @author jakub
 */
class PushUtils {

    public static function storePlayerId($player_id, $user_id) {
        $id = Db::querySingle('SELECT id FROM ' . TABLEPREFIX . 'push_devices WHERE device_id=?', $player_id);
        if (isset($id)) {
            Db::query('UPDATE ' . TABLEPREFIX . 'push_devices SET user_id=? WHERE id=?', $user_id, $id);
        } else {
            $data = array("user_id" => $user_id, "device_id" => $player_id, "created" => date("Y-m-d H:i:s"));
            Db::insert(TABLEPREFIX . 'push_devices', $data);
        }
    }

    public static function sendNotification($title, $message, $users_id) {
        $content = array(
            "en" => $message
        );
        $heading = array(
            "en" => $title
        );
        $web_buttons = array(array("id" => "id1", "text" => "test_text", "url" => "https://ldap.topescape.cz"));
        $fields = array(
            'app_id' => "007a2a99-738a-4c38-9c70-ae9a5f438c25",
            'include_external_user_ids' => $users_id,
            'data' => array("foo" => "bar"),
            'contents' => $content,
            'headings' => $heading,
        );
        ActionLogUtils::logWebHooks(json_encode(array("payload" => $fields, "post" => $_POST)));
        $response = json_decode(self::proceedNottification(json_encode($fields)), true);
        foreach ($users_id as $value) {
            $data = array("message_id" => $response['id'], "message_title" => $title,
                "message_body" => $message, "user_id" => $value, "raw_data" => json_encode($fields),
                "raw_response_data" => json_encode($response));
            Db::insert(TABLEPREFIX . 'push_messages', $data);
        }
    }

    private static function proceedNottification($fields) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic Mjk1NWQ4MzMtMDAzZS00YjE2LWIzY2YtODI5MDhhMDM3ZmZi'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $response = curl_exec($ch);
        print_r($response);
        curl_close($ch);
        return $response;
    }

    public static function storeWebHooks($payload) {
        ActionLogUtils::logWebHooks($payload);
    }

    public static function getDevicesByUserid($user_id) {
        $db = MysqliDb::getInstance();
        $db->where("user_id", $user_id);
        return $db->get("push_devices");
    }

    public static function delete_device($id, $user_id, $override = false) {

        $db = MysqliDb::getInstance();
        $db->where("id", $id);
        if (!$override)
            $db->where("user_id", $user_id);
        $player_id = $db->getValue("push_devices", "device_id");
        self::delete_device_remote($player_id);
        $db->where("id", $id);
        if (!$override)
            $db->where("user_id", $user_id);

        if ($db->delete("push_devices", 1)) {
            return true;
        }
    }

    private static function delete_device_remote($player_id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/players/" . $player_id . "?app_id=007a2a99-738a-4c38-9c70-ae9a5f438c25");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic Mjk1NWQ4MzMtMDAzZS00YjE2LWIzY2YtODI5MDhhMDM3ZmZi'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $response = curl_exec($ch);
        curl_close($ch);
        if (json_decode($response, true)['success'] != true)
            throw new Exception("Device has not beeen deleted on remote!");
        return $response;
    }

}
