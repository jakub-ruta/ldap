<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TokenUtils
 *
 * @author jakub
 */
class Token_oldUtils {

    public static function generateLoginTokenUtils() {
        $skupina_znaku = '1234567890';
        $pocet_znaku = strlen($skupina_znaku) - 1;
        $vystup = "";
        for ($i = 0; $i < 6; $i++) {
            $vystup = $vystup . $skupina_znaku[mt_rand(0, $pocet_znaku)];
        }
        return $vystup;
    }

    public static function authorizeAndGetRecaptcha($data) {
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=6LdqoK0UAAAAAP5gn4NBstvnsROmPO8iy3kwSXU3&response=" . $data;
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
        $obsah = curl_exec($handler);
        curl_close($handler);
        return json_decode($obsah, true);
    }

    public static function getRemoteToken($app, $fce = "get") {
        return Db::querySingle('SELECT token FROM ' . TABLEPREFIX . 'internal_remote_token '
                        . 'WHERE function=? AND adress=? ', $fce, $app);
    }

    public static function generateTokenForRemoteLogin($URL_params, $id) {
        $token = StringUtils::generate_string(100);
        $data = array("expire" => time() + 180, "app" => $URL_params, "token" => $token, "user_id" => $id);
        Db::insert(TABLEPREFIX . 'tokens', $data);
        return $token;
    }

    public static function getUserToken($user_token) {
        $token = Db::queryOne('SELECT * FROM ' . TABLEPREFIX . 'tokens '
                        . 'WHERE  token=? ', $user_token);
        if ($token['expire'] < time() || $token['valid'] == 0) {
            throw new ErrorException("token expired");
        }
        return $token;
    }

    public static function remoteTokenValid($function, $token) {
        $token_1 = Db::querySingle('SELECT token FROM ' . TABLEPREFIX . 'internal_remote_token '
                        . 'WHERE function=? AND token=? ', "get", $token);
        if ($token_1 != "" && $token_1 != null) {
            return true;
        }
        return false;
    }

    public static function verifyPushToken($token) {
        $token_1 = Db::querySingle('SELECT token FROM ' . TABLEPREFIX . 'internal_remote_token '
                        . 'WHERE function=? AND token=? ', "send", $token);
        if ($token_1 != "" && $token_1 != null) {
            return true;
        }
        return false;
    }

    public static function verifyGetToken($token) {
        $token_1 = Db::querySingle('SELECT token FROM ' . TABLEPREFIX . 'internal_remote_token '
                        . 'WHERE function=? AND token=? ', "get", $token);
        if ($token_1 != "" && $token_1 != null) {
            return true;
        }
        return false;
    }

    public static function verifySetToken($token) {
        $token_1 = Db::querySingle('SELECT token FROM ' . TABLEPREFIX . 'internal_remote_token '
                        . 'WHERE function=? AND token=? ', "set", $token);
        if ($token_1 != "" && $token_1 != null) {
            return true;
        }
        return false;
    }

    public static function updateToken($function, $adress, $token, $new_token) {
        Db::query('UPDATE  ' . TABLEPREFIX . 'internal_remote_token SET token=?, next_update_timestamp=? '
                . 'WHERE function=? AND adress=? AND token=? ', $new_token, time() + 60 * 60 * 2, $function, $adress, $token);
    }

    public static function getAllOldTokens() {
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'internal_remote_token '
                        . 'WHERE next_update_timestamp<? AND support_auto_update=? ', time(), 1);
    }

    public static function getSettingsTokenFor($adress) {
        $token = Db::querySingle('SELECT token FROM ' . TABLEPREFIX . 'internal_remote_token '
                        . 'WHERE function=? AND adress=? ', "setup", $adress);
        if ($token != "" && $token != null) {
            return $token;
        }
        throw new tokenException("no token");
    }

}
