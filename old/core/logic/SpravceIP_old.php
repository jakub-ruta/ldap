<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SpravceIP
 *
 * @author jakub
 */
class SpravceIP {

    protected static $IP_ban_text = "Your IP has been banned for wrong login! Try it later!";
    protected static $IPbanTime = 60 * 60;

    public static function isIPBanned() {
        $banned_ip = Db::queryOne(' SELECT * FROM ' . TABLEPREFIX . 'ip_bans '
                        . 'WHERE ip=?', self::getUserIpAddr());

        if ($banned_ip['deny'] == 1 && strtotime($banned_ip['ban_expire']) > time()) {

            return true;
        }
        return false;
    }

    public static function getUserIpAddr() {
        return $_SERVER['REMOTE_ADDR'];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    public static function getUserBrowser() {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    public static function getBannedIP() {
        return Db::queryOne(' SELECT * FROM ' . TABLEPREFIX . 'ip_bans '
                        . 'WHERE ip=?', self::getUserIpAddr());
    }

    public static function banIPWrongLogin() {
        $banned_ip = self::getBannedIP();
        if (isset($banned_ip['id'])) {
            Db::query('UPDATE ' . TABLEPREFIX . 'ip_bans SET deny=1, allow_write_ticket=1, internal_note=NULL, '
                    . 'visible_note=?, ban_expire=?, replicate=0 WHERE id=?',
                    self::$IP_ban_text, date("Y-m-d H:i:s", time() + self::$IPbanTime), $banned_ip['id']);
            $data = array("ip_ban_id" => $banned_ip['id'], "unban_time" => date("Y-m-d H:i:s", time() + self::$IPbanTime),
                "visible_text" => self::$IP_ban_text, "admin" => "auto");

            Db::insert(TABLEPREFIX . 'ip_ban_history', $data);
        } else {
            $data = array("ip" => self::getUserIpAddr(), "deny" => 1, "allow_write_ticket" => 1,
                "visible_note" => self::$IP_ban_text, "ban_expire" => date("Y-m-d H:i:s", time() + self::$IPbanTime));

            Db::insert(TABLEPREFIX . 'ip_bans', $data);
            $data = array("ip_ban_id" => Db::getLastId(), "unban_time" => date("Y-m-d H:i:s", time() + self::$IPbanTime),
                "visible_text" => self::$IP_ban_text, "admin" => "auto");
            Db::insert(TABLEPREFIX . 'ip_ban_history', $data);
        }
    }

}
