<?php

class settings {

    private $db_name = "ldap.topescape.local";
    private $db_server = "localhost";
    private $db_user = "development";
    private $db_password = "fC9Xo9mF8dRAAEoj";

    public function __construct() {

        define('APPDATABASE', $this->db_name);
        define('TABLEPREFIX', 'login_');
        define('SESSIONID', 'login_ses');
        define('APPNAME', 'New Login system');
        define('SESSIONDOMAIN', 'ldap.topescape.local');
        define('MASTERDOMAIN', 'topescape.cz');
        define('ALWAYSSHOWDEBUG', false);
        define('ENABLELOGQUERIES', true);
        define('APPVERSION', "0.1.5 dev (1.1.2021)");
        define('MASTERFOLDER', 'v10');
        define('GOOGLELOGIN', "929810137554-q72h3v2q1lnc3l6enf1rkjk8rvar993p.apps.googleusercontent.com");
    }

    function getDb_name() {
        return $this->db_name;
    }

    function getDb_server() {
        return $this->db_server;
    }

    function getDb_user() {
        return $this->db_user;
    }

    function getDb_password() {
        return $this->db_password;
    }

}
